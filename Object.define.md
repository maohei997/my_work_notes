#### Object.property方法

Object.defineProperty() 接收三个参数



````javascript
let person = {

	name: ' 张三 ',

	sex: ' 男 '

}
let number = 19

Object.defineProperty(给哪个对象person，添加的属性叫什么'age'，配置项{value:18})

Object.defineProperty(person,'age',{
    value:18,
    enumerable:true, //可列举的 控制属性是否可枚举，默认false
    writable:true,//控制属性是否可以被修改，默认为false
    configurable:true //控制属性是否可以被删除，默认为false
})

Object.defineProperty(person,'age',{
    get(){ //当有人读取person的age属性时，get函数(getter)会被调用，且返回值就是age的值
        return number //某个定义的值
    },
    set(value){ //当有人修改person的age属性时，set函数(setter)会被调用，且会收到具体修改值
        number = value
    }
})
````



添加的属性 不会被枚举（遍历）



#### 数据代理

通过一个对象代理对另一个对象中属性的操作

1. Vue中的数据代理

   通过vm对象来代理data对象中属性的操作(读、写)

2.  Vue中数据代理的好处

   更加方便的操作data中的数据

3. 基本原理

   通过Object.defineProperty()把data对象中所有属性添加到vm上。

   为每一个添加到vm上的属性，都指定一个getter/setter

   在getter/setter内部去操作（读、写）data中对应的属性

vm自身会有 _data  里面是数据劫持

## Observer

````javascript
let data = {a:1,b:2}

//创建一个监视的实例对象  用于监视data中属性的变化
const obs = new Observer(data)
console.log(obs)
//准备一个vm实例对象
let vm = {}
vm._data = data = obs

function Observer(obj){
	//汇总对象中所有的属性形成一个数组
    const keys = Object.keys(obj)
    //遍历
    keys.forEach(k =>{
        Object.defineProperty(this,k,{
            get(){
                return obj[k]
            },
            set(val){
                console.log(`${k}被改了,解析模板 对比虚拟DOM  页面更新`)
                obj[k] = val
            }
        })
    })
}
````

