## 关于Axios在vue中 拦截器和Promise封装

#### 一、拦截器

在src目录下或者其他位置新建network文件夹 目录层级为下（仅仅是便于管理）

[![IYvk6I.png](https://z3.ax1x.com/2021/11/09/IYvk6I.png)](https://imgtu.com/i/IYvk6I)

在 **network/interceptor.js**中

````javascript
import base from '../utils/baseUrl'
import { Message } from 'element-ui'
import router from '../router/index'

const install = (Vue,vm) => {
  //Vue就是Vue对象（不是创建出的实例）
  //vm就是页面中的vue实例每一个this,在这里可以访问到this的所有属性
  //baseUrl 为项目代理前缀
  Vue.prototype.$axios.defaults.baseURL = base.baseUrl;
  Vue.prototype.$axios.defaults.timeout = 60000;
  //请求拦截器
  Vue.prototype.$axios.interceptors.request.use(
    config => {
      //可以在此判断要不要加Token
      //if(vm.$store.state.token){config.headers['Token'] = vm.$store.state.token}
      config.headers['Token'] = vm.$store.state.token
      return config
    },
    err => {
      Promise.reject(err)
    }
    )
  //响应拦截器
  Vue.prototype.$axios.interceptors.response.use(
    res => {
      let {code,data,msg} = res.data
      if(code !== 0){
        Message.warning(msg || '网络异常')
        return false
      }
      return data
    },
    err => {
      if(err?.response?.status){
        let {status} = err.response
        if(status === 401){
          if(location !== '#/login'){
            router.replace('/login')
            return false;
          }
        }else if(status === 502){
          Message.warning('服务器正在部署,请稍候~')
          return false;
        }else{
          Message.warning('服务异常,状态码 ==>>>>',status)
        }
      }else{
        Message.warning('服务异常')
      }
      return false;
    }
  )
}


export default {
  install
}
````

#### 二、axios封装

在 **network/api/index.js**中 进行axios请求加工

````javascript
import axios from "axios"
import qs from 'qs' //按需引入
const install = (Vue,vm) => {

  function get (url,params){
    return new Promise((resolve,reject) => {
      axios.get(url,{params}).then(res => {
        resolve(res)
      }).catch(err => {
        reject(err)
      })
    })
  }

  function post (url,params,headers){
    // 某些请求不用JSON格式 就需要判断
    // let _params = params
    // if(headers === undefined){
    //   axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    //   _params = qs.stringify(params);
    // }else{
    //   axios.defaults.headers.post['Content-Type'] = 'application/json; charset=UTF-8';
    // }
    return new Promise((resolve, reject) => {
      axios.post(url, params, {indices: false}).then((res) => {
          resolve(res);// 返回请求成功的数据 data
      }).catch((err) => {
          reject(err);//返回失败的错误信息
      });
    });
  }
  let api = {
    get,post,
    //登录
    gate_login: (data) => post('请求的URL', data),
      //继续增加接口
      ......
  }
  Vue.prototype.$api = api  //挂载到vue 原型 在页面中调用就用this.$api.gate_login({xxx:xxx}).then(res => {console.log(res)})
}

export default {
  install
}
````

#### 三、关键点 main.js 引入使用

默认axios 拦截器为直接引入使用,而无法使用Vue.use() 方法去挂载到Vue，这是因为axios中没有install方法

详见其他博主文章https://blog.csdn.net/ZYS10000/article/details/107246076/

uview 介绍Vue.use()  https://www.uviewui.com/components/vueUse.html

这里拦截器和API封装 用install 包裹一层的封装方法，拓展性更高

````javascript
//main.js
......//该文件中其他代码
//这里需要把脚手架的挂载方法拆分一下
//原本是
new Vue({
    router,
    store,
    ...,//其他代码
    render: h => h(App)
}).$mount('#app')


//如果用了install 方法封装的拦截器和API 方法那就要改造成
const app = new Vue({
    router,
    store,
    ...,//其他代码
    render: h => h(App)
})

//必须写在 Vue实例app 和 app挂载方法之间，否则会有错误或者不可用,主要是为了引入这个app 也就是拦截器和api中的vm参数

import httpInterceptor from './network/interceptor'
Vue.use(httpInterceptor,app)

import httpApi from './network/api/index'
Vue.use(httpApi,app)

app.$mount('#app')
````

#### 四、页面中使用

````javascript
this.$api.gate_login({username,password}).then(res => {
	console.log('=====>>>>>>>>>>>',res)
})
````

