## React脚手架学习随笔

#### 一、React脚手架安装

> //全局安装react脚手架
>
> npm i -g create-react-app
>
> //查看版本
>
> create-react-app -V 或者 --version
>
> //创建项目
>
> create-react-app 项目名

#### 二、脚手架各个文件包含什么

node_modules文件夹 不必多说

public 静态文件 https://www.jianshu.com/p/83d540245b07

​            **favicon.ico :** 这个是网站或者说项目的图标，一般在浏览器标签页的左上角显示。

​            **index.html :** 首页的模板文件，我们可以试着改动一下，就能看到结果。

src文件夹 项目的主要文件夹 

src--->index.js  主要入口文件 参考vue中的`main.js`

​        |----->**App.js**  这个文件相当于一个方法模块，也是一个简单的模块化编程。

​        |----->**index.css**  主要css文件

​        |----->**serviceWorker.js:** 这个是用于写移动端开发的，PWA必须用到这个文件，有了这个文件，就相当于有了离线浏览的功能。

#### 三、JSX语法

https://blog.csdn.net/a153375250/article/details/53434299

#### 四、React基本认识

在App.js中

````javascript
import React, { Component } from 'react';
import './App.css';

//1.简单数据绑定
//react中插值语法为  {xxx}  一对大括号
const date = Date.now();
//另一种声明方式 结合jsx语法
const element = (
	<span>
    	我是另一种方式
    </span>
);

//2.属性也可以绑定
const pId = 'ppId';

//3. {} 中也可以执行任何的js代码， 三目运算、函数调用等
const num = 9;

//4. jsx 防注入 
const dElement = '<h3>aaaaaa</h3>'

//5.显示一个数组
const arr = [
    <div>第一个元素</div>,
    <div>第二个元素</div>,
    '<div>第三个元素</div>',
];

const arrCities = ['北京','天津','上海'];

class App extends Component {
    getDay(num){
        return (
        	<span>
            	周{ parseInt(num % 7) }
            </span>
        
        )
    }
    
  render () {
    return (
      <div className='App'>
        <h1 id={ pId }>hahhahaha</h1>
        <p>当前时间是：{date} </p>
		<div className="article">
            { element }
        </div>
		<hr/>
        <p>{ this.getDay(333) }</p>
		<p>{ num > 8 ? 'lalala' : 'null' }</p>
		<div>{ dElement }</div>
		<hr/>
        <div>
        	这是数组的绑定展示：
			{ arr }
        </div>
		<hr/>
        <div>
            所有的城市：
			<ul>
            	{
            		arrCities.map(v => {
            			return (
							<li>{ v }</li>
            			)
        			})
        		}
            </ul>
		</div>
      </div>
    )
  }
}

export default App;
````

**函数组件**

````javascript
//在src/新建components文件夹/新建Welcome.js文件
//在里面写如下代码
import React from 'react'

export default function Welcome(props){
    return (
        <div>
            <p>welcome to { props.name }</p>
        </div>
    )
}

//在App.js
import Welcome from './componetns/Welcome';
class App extends Component {
  render () {
    return (
      <div className="App">
        <h1>hahhahaha</h1>
        <hr/>
        {/*函数组件*/}
        <Welcome name="react"/>
      </div>
    )
  }
}

````

**类组件**

````javascript
//在src/新建components文件夹/新建JSXDemo.js文件
//在里面写如下代码
import React, { Component } from 'react';


const date = Date.now();
const element = (
    <span>
        我是另一种方式
    </span>
);
const pId = 'ppId'
const num = 5;
const dElement = '<h3>aaaaa</h3>'
const arr = [
    <div>第一个</div>,
    <div>第二个</div>,
    '<div>第三个</div>'
];
const arrCities = ['a', 'b', 'c'];

class JSXDemo extends Component {

    constructor(opt) {
        super(opt);
    }

    getDay(num){
        return (
          <span>
            周{ parseInt(num % 7) }
          </span>
        )
      }

      
    render() {
        return (
            <div>
                <p>当前时间是：{date} </p>
                <div className="article">
                    {element}
                </div>
                <hr />
                <p>{this.getDay(333)}</p>
                <p>{num > 8 ? 'lalala' : 'null'}</p>
                <div>
                    {dElement}
                </div>
                <hr />
                <div>
                    数组的绑定展示
                    {arr}
                </div>
                <hr />
                <div>
                    数组的元素
                    <ul>
                        {
                            arrCities.map(v => {
                                return (
                                    <li>{v}</li>
                                )
                            })
                        }
                    </ul>
                </div>
            </div>
        )


    }
}
export default JSXDemo;


//在App.js
import React, { Component } from 'react';
import './App.css';

import JSXDemo from './components/JSXDemo';



class App extends Component {
  
  render () {
    return (
      <div className="App">
        <h1>hahhahaha</h1>
        <hr/>
        {/*类组件*/}
        <JSXDemo/>
        
      </div>
    )
  }
}

export default App;

````

**React组件的状态state**

​																																**组件的生命周期**[![B9DdFU.png](https://s1.ax1x.com/2020/10/21/B9DdFU.png)](https://imgchr.com/i/B9DdFU)



![具体生命周期](https://images2015.cnblogs.com/blog/588767/201612/588767-20161205190022429-1074951616.jpg)

[![B9cpCT.jpg](https://s1.ax1x.com/2020/10/21/B9cpCT.jpg)](https://imgchr.com/i/B9cpCT)

````javascript
//在src/新建PropsDemo.js
import React, { Component } from 'react'

export default class PropsDemo extends Component {
    // 定义组件内部私有数据
    // 自动合并所有状态，单独进行修改

    constructor(opt){
        super(opt);
        this.state = {
            num:0,
            phone:110
        };
    }
    // 组件渲染到页面后，每隔1s后修改页面的num
    componentDidMount(){
        // setInterval(() => {
        //     //如果直接修改的话
        //     // this.setState({ num: 1 });
        //     // 如果需要以来之前的状态做相关计算，需要使用下面的这种方法进行修改  this.setState()方法修改state的值
        //     this.setState((preState) => {
        //         return {num: preState.num + 1}
        //     })
        // },1000)
    }

    render() {
        return (
            <div>
                <p>当前属性：kk-{ this.props.kk }</p>
                <p>num-{ this.state.num }</p>
                <p>phone-{ this.state.phone }</p>
            </div>
        )
    }
}

//App.js
import React, { Component } from 'react';
import './App.css';

import PropDemo from './components/PropsDemo';


class App extends Component {
  
  render () {
    return (
      <div className="App">
        <PropDemo kk="hahahahaha"/>
      </div>
    )
  }
}

export default App;

````

**生命周期详解（常用）**

````javascript
//组件ReactLife
import React, { Component } from 'react';

class ReactLife extends Component {
    // 组件最开始首先执行 构造函数
    constructor(props) {
        super(props);
        this.state = {
            name:'嘎嘎',
            age:19
        }
    }

    componentWillMount() {
        // 组件将要被挂载到页面上，事件结束之后，执行render
        console.log('componentWillMount')

    }

    componentDidMount() {
        // 组件已经渲染完成,此时可以访问到页面上的DOM元素
        // 此方法用到的非常频繁，一般用于进行网络请求，获取后台数据。
        console.log('componentDidMount')

        setTimeout(() => {
            this.setState({ age : 20 });
        }, 1000);
    }

    componentWillReceiveProps(nextProps) {
        // 父组件修改当前组件的属性时，会触发此方法。
        console.log(nextProps,'componentWillReceiveProps')
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     // 只会返回两个值，true / false 
    // }

    componentWillUpdate(nextProps, nextState) {
        // 组件将被更新之前
        console.log('componentWillUpdate',nextProps,nextState)
    }

    componentDidUpdate(prevProps, prevState) {
        // 组件已经更新完成
        console.log('componentDidUpdate')
    }

    componentWillUnmount() {
        // 组件将要移除、卸载
        console.log('componentWillUnmount')
    }

    render() {
        return (
            <div>
                {this.state.age} - {this.props.kk}
            </div>
        );
    }
}

export default ReactLife;


//APP.js
import React, { Component } from 'react';
import './App.css';

import ReactLife from './components/ReactLife';


class App extends Component {
  constructor(opt){
    super(opt);
    this.state = {
      kk: 'lalala',
      isShowLife:true
    }
  }

  componentDidMount(){
    setTimeout(() => {
      this.setState({kk:'lueluelue'});
    }, 2000);

    setTimeout(() => {
      this.setState({isShowLife:false});
    }, 4000);
  }

  render () {
    return (
      <div className="App">
        {
          this.state.isShowLife && <ReactLife kk={this.state.kk}/>
        }
        
      </div>
    )
  }
}

export default App;

````

**简单时钟的例子**

````javascript
//组件Clock
import React, { Component } from 'react'

export default class Clock extends Component {
    constructor(opt){
        super(opt);

        this.state = {
            now: new Date().toLocaleTimeString(),
            timer:null
        }
    }

    componentDidMount(){
       this.setState({
           timer : setInterval(() => {
                this.setState({
                    now : new Date().toLocaleTimeString()
                })
            }, 1000)
       }) 
    }

    componentWillUnmount(){
        clearInterval(this.state.timer);
    }
    
    render() {
        return (
            <div>
                <p>当前时间是: { this.state.now }</p>
            </div>
        )
    }
}


//App.js
import React, { Component } from 'react';
import './App.css';
import Clock from './components/Clock'

class App extends Component {
  constructor(opt){
    super(opt);
    this.state = { }
  }
  render () {
    return (
      <div className="App">
        <Clock/>
      </div>
    )
  }
}

export default App;

````

**事件**

````javascript
//在组件Event.js
import React, { Component } from 'react'

export default class Event extends Component {
    constructor(opt){
        super(opt);
        this.handlerClick = this.handlerClick.bind(this);
    }
    
    handlerClick(e){
        console.log(this)
    }

    handlerClick2(){
        console.log(this)
    }

    addNum(a,b){
        console.log(this);
        console.log(a+b)
    }
    // stage 3
    demoFun = (e) => {
        console.log(this)
    }

    render() {
        return (
            <div>
                <button onClick={ this.handlerClick }>
                    简单按钮
                </button>
                {/* dom中 */}
                <button onClick={ this.handlerClick2.bind(this) }>
                    简单按钮2222
                </button>
                <button onClick={ this.addNum.bind(this,3,4) }>
                    传递参数
                </button>
                <button onClick={ this.demoFun }>
                    私有变量
                </button>
            </div>
        )
    }
}



//App.js
import React, { Component } from 'react';
import './App.css';

import Event from './components/Event'

class App extends Component {
  constructor(opt){
    super(opt);
    this.state = {
      kk: 'lalala',
      isShowLife:true
    }
  }
  render () {
    return (
      <div className="App">
        <Event/>
      </div>
    )
  }
}

export default App;

````

