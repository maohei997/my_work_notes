## Window.location各个参数

假设这样一个URL地址：http://www.php230.com :80/fisker/post/0703/window.location.html?ver=1.0&id=6#imhere

我们可以用javascript获得其中的各个部分
1, window.location.href
整个URl字符串(在浏览器中就是完整的地址栏)
本例返回值:
代码如下 复制代码
http://www.php230.com :80/fisker/post/0703/window.location.html?ver=1.0&id=6#imhere

2,window.location.protocol
URL 的协议部分
本例返回值:http:

3,window.location.host
URL 的主机部分
本例返回值:www.php230.com

4,window.location.port
URL 的端口部分
如果采用默认的80端口(update:即使添加了:80)，那么返回值并不是默认的80而是空字符
本例返回值:""

5,window.location.pathname
URL 的路径部分(就是文件地址)
本例返回值:/fisker/post/0703/window.location.html

6,window.location.search
查询(参数)部分
除了给动态语言赋值以外，我们同样可以给静态页面,并使用javascript来获得相信应的参数值
本例返回值:?ver=1.0&id=6

7,window.location.hash
锚点
本例返回值:#imhere

8,location.assign()
载入一个新地址/文档

9,location.reload()

重新载入当前地址/文档

10,location.replace()

用新文档/地址替换当前地址/文档



