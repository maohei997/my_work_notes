## Egg.js入门

### egg.js 安装以及搭建项目

````javascript
//安装
    npm i egg-init -g //安装到全局
//搭建项目
	npm init egg --type=simple

	npm i 
// 启动项目
    npm run dev    
````

### egg demo 各个文件

npm init egg --type=simple之后会生成一个这样的结构

````javascript
项目名
 |
  ----app
	   |
       -----controller (编写控制器 MVC中的C)
	   -----public (存放静态网页和图片)
	   -----router.js

  ----config
  		|
        -----config.default.js
		-----plugin.js
  
  ----logs
  .....
  
  ----run 
  .......
````

### 如何编写一个controller？

````javascript

//默认自动生成
'use strict'; //(写不写都行)
const Controller = require('egg').Controller; //从egg引入它的Controller

class HomeController extends Controller {
    
    async index (){  //必须使用async await
        
        //this.ctx; 可以获取到当前请求的上下文对象，通过此对象可以便捷的获取到请求和响应的属性与方法。
        const {ctx} = this;
        ctx.body = 'hi,egg';
    }
}

module.exports = HomeController; //暴露出去这个controller



//自己书写
const Controller = require('egg').Controller;

class FruitsController extends Controller {

  async index (){
    let query = this.ctx.request.query;
    console.log(query);//{index:100}
    this.ctx.body = `传递的值是${query.index}`; //http://localhost:7001/fruit?index=100
  }

  async getId(){
    this.ctx.body = `传递的ID值为${this.ctx.params.id}`;//http://localhost:7001/fruit/100
  }
    
   /**
   * 在controller中可以处理表单提交的数据
   * 获取post请求的参数：this.ctx.request.body
   *CSRF值跨站请求伪造，Egg对post请求做了一些安全验证，可以再config.default.js中，通过下面的设置验证。直接添加即可。
   * config.security = {
   		
   		csrf:{
   			enable:false,
   		}
   
   }
   */
    async createPage (){
     	this.ctx.body = `<form method='post' action='/createfruit'>
      						<input name='fruitName'>
      						<button>添加</button>
    					</form>`   
    	}
    
    async createFruit(){
        let fruit = this.ctx.request.body;
        fruitList.push(fruit.fruitName);
        this.ctx.body = '添加成功';
      }
    
}

module.exports = FruitsController;

````

### 在router.js中注册这个controller

````javascript
'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  router.get('/fruit',controller.index.index); //对应this.ctx.request.query
  router.get('/fruit/:id',controller.index.getId); //对应this.ctx.params.id
  router.get('/createfruit',controller.index.createPage);
  router.post('/createfruit',controller.index.createFruit);
    
  /*
   *还有restful风格的url可以简化路由文件
   *router.resources('posts','/api/posts',controller.posts);//一个方法定义增删改查
  */
   router.resources('fruit','/fruit',controller.index)
   /*
   	相对应的在controller中修改方法名 方法名对应相应的resources方法
        method			path			route name			controller.action
        
        GET				/posts			posts				app.controllers.posts.index
        
        GET				/posts/new		new_post			app.controllers.posts.new
        
        GET				/posts/:id		post				app.controllers.posts.show
        
        GET				/posts/:id/edit	edit_post			app.controllers.posts.edit
        
        POST			/posts			posts				app.controllers.posts.create
        
        PUT				/posts/:id		post				app.controllers.posts.update
        
        DELETE			/posts/:id		post				app.controllers.posts.destroy
   	
   */
    /*
    	相应的要在controller中修改方法名 一一对应
    	async index (){
    		this.ctx.body = fruitList;
    	}  //路由中就是 http://localhost:7001/index
    	async new (){
    		this.ctx.body = `<form method='post' action='/index'>
                              <input name='fruitName'>
                              <button>添加</button>
                            </form>`
    	}  //路由中就是 http://localhost:7001/index/new
    	async create (){
    		  let fruit = this.ctx.request.body;
              fruitList.push(fruit.fruitName);
              // this.ctx.body = '添加成功';
              this.ctx.redirect('/index');
    	} 
    	async show(){
            this.ctx.body = `展示${fruitList[this.ctx.params.id]}`
          };//路由中就是 http://localhost:7001/index/1
    */
};
````

### Egg插件

Egg通过插件来实现功能的扩展。

`1. egg-view-nunjucks : nunjucks模板插件`

**npm i --save egg-view-nunjucks**

````javascript
在 plugin.js中写入
'use strict';

/** @type Egg.EggPlugin */
module.exports = {
  // had enabled by egg
  // static: {
  //   enable: true,
  // }
  nunjucks:{
    enable:true,
    package:'egg-view-nunjucks',
  }
};


在config.default.js中写入
config.view = {
    defaultViewEngine:'nunjucks'
}



//在controller中

async index() {
    const { ctx } = this;
    await ctx.render("index",{fruits:['香蕉','苹果','鸭梨']});
  }

//在view文件夹中的index.html中
....
    <body>
      <h1>水果</h1>
      <ul>
        {% for item in fruits%}
          <li>{{item}}</li>
        {% endfor %}
      </ul>
    </body>
....
````



`2. egg-cors 跨域请求配置插件`

````javascript
//1.安装 npm install --save egg-cors
//2.在 plugin.js文件中引入插件
//3.在config.default.js文件中配置egg-cors插件

//在plugin.js中写入
cors:{
    enable:true,
    package:'egg-cors'
}
//在config.default.js中写入
config.cors = {
    origin:'*',
    allowMethods:'GET,POST,PUT,PATCH,DELETE,HEAD'
}

//至此其他项目就可以访问到这里定义的数据了
````

 ## http

#### 请求

get  post  delete  put ....

#### 响应

状态 200 404 500....

数据:  json格式   javascript 对象表示法

````json
{
    "name":"小明",
    "age":2
}

["ww","aa"]
````

#### 无状态

访问一个电商网站

1.登录

2.购物车

**服务器如何识别用户**

`1.使用cookie与session识别用户 `

使用session保持用户登录状态

1.登录后可以访问首页

2.未登录，访问首页跳转到登录

`2.使用JWT（Json Web Token）识别用户`

概述

1.   json : js对象表示法，用js的对象来表示数据格式，例如 [{'name':'xxx'},{'age':1}]

2.  token: 加密的字符串(标识), 客户端带着token 向服务器发送请求，以证明自己的身份

#### 如何生成token

1. 安装 egg-jwt 插件  npm i --save egg-jwt

2. 在plugin.js中引入插件
   jwt :{
      enable:true,
      package:''egg-jwt''

   },

3. 配置config.default.js 文件, 设置secret; 注意 secret 不能泄露。
   config.jwt = {secret:'xxxxx'}

````javascript
//在jwt.js文件中
const Controller = require('egg').Controller;

class JwtController extends Controller {
  async index(){
    let user = {
      username:'xiaoming'
    }

    //egg-jwt
    let token = this.app.jwt.sign(user,this.app.config.jwt.secret); //签名
    //this.ctx.body = token;//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InhpYW9taW5nIiwiaWF0IjoxNjI1NTM5OTIyfQ.VpidQRLo23eVcje_3dh779whEJODybTYBgMVyGZ45GE
    //let decode = this.app.jwt.verify(token,this.app.config.jwt.secret); //验证
    //this.ctx.body = decode;//{username: "xiaoming",iat: 1625540258 //时间戳}
      
      
    //一般来说用try catch语句来优雅的验证token
      try{
        let decode =this.app.jwt.verify(token,this.app.config.jwt.secret);//验证
        this.ctx.body = decode;//{username: "xiaoming",iat: 1625540258 //时间戳}
      }catch(e){
          this.ctx.body = 'token未能通过验证';
      }
  }
}

module.exports = JwtController;

//在router.js文件中注册
  router.get('/jwt',controller.jwt.index);
````

#### 验证流程

1.  根据用户信息，生成token，并响应给客户端
2. 客户端存储在localstorage中
3. 客户端每次请求数据，请求头携带token
4. 服务器接收请求，验证请求头的token，验证成功则相应数据。

### 关于token一个demo

**默认下载好各自的插件和依赖  并且在 plugin.js 和  config.default.js 中都配置好了**

在egg项目中的controller中新建 jwt.js文件

````javascript
const Controller = require('egg').Controller;

class JwtController extends Controller {

  async doLogin(){
    let user = this.ctx.request.body.user; //user.username   user.password
    if(user.username === 'xiaoming' && user.password === '123'){
      let user_jwt = {username:user.username}
      let token = this.app.jwt.sign(user,this.app.config.jwt.secret);
      this.ctx.body = {
        code:1,
        token:token
      }
    }else{
      this.ctx.body = {
        code:0,
        msg:'用户名或者密码错误'
      }
    }
  }

  async getMessage(){
    let token = this.ctx.request.header.token;
    try{
      let decode = this.app.jwt.verify(token,this.app.config.jwt.secret);
      this.ctx.body = 'hello jwt';
    }catch(e){
      this.ctx.body = 'token未能通过验证';
    }
  }

}

module.exports = JwtController;
````

在router.js中注册

````javascript
  router.post('/jwtlogin',controller.jwt.doLogin);
  router.get('/jwtmessage',controller.jwt.getMessage);
````

在vue脚手架项目中  新建首页和登录页

Home.vue代码如下

````javascript
<template>
  <div>
    <h1>首页</h1>
    <button @click="getMessage">获取</button>
    <button @click="logout">注销</button>
  </div>
</template>

<script>
import axios from 'axios';
export default {
  data(){
    return {

    }
  },
  methods:{
    getMessage(){
      let token = localStorage.getItem('token');
      axios.get('http://127.0.0.1:7001/jwtmessage',{
        headers:{token}
      })
      .then(res => {
        console.log(res.data)
      })
    },
    logout(){
      localStorage.setItem('token','');
      location.reload();
    }
  }
}
</script>

<style>

</style>
````

Login.vue 代码如下

````javascript
<template>
  <div>
    <h1>登录</h1>
    <form @submit.prevent="doLogin">
      <input type="text" v-model="user.username">
      <input type="password" v-model="user.password">
      <button>登录</button>
    </form>
  </div>
  
</template>

<script>
import axios from 'axios'
export default {
  data(){
    return {
      user:{
        username:'',
        password:''
      }
    }
  },
  methods:{
    doLogin(){
      axios.post('http://127.0.0.1:7001/jwtlogin',{user:this.user})
      .then(res => {
        if(res.data.code == 1){
          localStorage.setItem('token',res.data.token);
          console.log(res.data.token);
          this.$router.push('/home')
        }
      })
      
    }
  }
}
</script>

<style>

</style>
````

@/router/index.js 代码如下

````javascript
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path:'/',
    component:()=> import('../views/index.vue')
  },
  {
    path:'/home',
    component:()=> import('../views/Home.vue')
  },
  {
    path:'/login',
    component:()=> import('../views/Login.vue')
  }
]

const router = new VueRouter({
  routes
})

//路由守卫
router.beforeEach((to,from,next) => {
  if(to.path === '/login'){
    next();
  }else{
    if(localStorage.getItem('token')){
      next();
    }else{
      next('/login');
    }
  }
})

export default router

````



### 中间件

#### 在egg中定义中间件

1. 在app下创建middleware目录

2. 在middleware中创建 js 文件

   ````javascript
   function checktoken(){
     return async function(ctx,next){
       try{
         // 获取token
         let token = ctx.request.header.token;
         // 校验
         let decode = ctx.app.jwt.verify(token,ctx.app.config.jwt.secret);
         if(decode.username){
           await next();
         }else{
           ctx.body = {
             code:0,
             msg:'用户校验失败'
           }
         }
       }catch(e){
         ctx.body = {
           code:0,
           msg:'token未通过'
         }
       }
       
     }
   }
   
   module.exports = checktoken;
   ````

3.  定义好以后在router.js中使用

   在路径和控制器中间添加即可

   ````javascript
   router.get('/jwtmessage',app.middleware.checktoken(),controller.jwt.getMessage);
   ````

   

### egg-sequelize 操作数据库

 	 **!!!!!!!!!!! 需要先下载mysql !!!!!!!!!!!** 

1. 安装   npm i --save egg-sequelize mysql2

2. 在plugin.js文件中引入插件

   ````javascript
   sequelize:{
       enable:true,
       package:'egg-sequelize'
   }
   ````

3. 在config.default.js中 配置数据库连接

   ````javascript
   config.sequelize = {
       dialect:'mysql', //确定数据库的类型
       database:lch,//数据库名称
       host:'localhost',//地址
       port:3306,//
       username:'root',//用户名
       password:'123456',//密码
     };
   ````

   

4. 在app/model 文件中创建数据模型

   ````javascript
   //在app文件夹下新建model文件夹
   //新建xxx.js  内容如下
   module.exports = app => {
       const {STRING} = app.Sequelize;
       const Xxx = app.model.define('xxx',{
           // id 自动生成
           name:STRING,
       })
       return Xxx;
   }
   
   ````

   

5. 根目录文件中添加app.js文件,初始化数据库

   ````javascript
   module.exports = app =>{
   	app.beforeStart(async function(){
           //await app.model.sync({force:true}); //开发环境使用，会删除数据
           //sync  方法会根据模型去创建表
           await app.model.sync({})
       })
   }
   ````

6. 新建controller下xxx文件这里用clazz.js

   ````javascript
   const Controller = require('egg').Controller;
   
   class ClazzController extends Controller {
       //restful风格的 create增destroy删update改index查
       
       async index (){
           this.ctx.body = '查看数据';
       }
       
       async create (){
           this.ctx.body = '查看数据';
       }
       
       async destroy (){
           this.ctx.body = '查看数据';
       }
       
       async update (){
           this.ctx.body = '查看数据';
       }
       
   }
   
   module.exports = ClazzController;
   ````

7. 在router.js中注册此控制器

   ````javascript
   router.resources('clazz','/clazz',controller.clazz);
   ````

   这一步结束后，启动服务，在POSTMAN可以正常访问对应的路由拿到对应的值。

8. 在Controller中实现数据的增删改查

   ````javascript
   this.app.model.Clazz.findAll();//查询数据
   this.app.model.Clazz.findAll({where:{id:1}});//通过where设置查询条件
   this.app.model.Clazz.create({name:'xx'});//添加数据
   this.app.model.Clazz.update({name:'xx'},{where:{id:1}});//通过条件修改数据
   this.app.model.Clazz.destroy({where:{id:1}});//通过条件删除数据
   ````

   具体代码

   ````javascript
   const Controller = require('egg').Controller;
   
   class ClazzController extends Controller {
   
     //restful : index/create/destroy/update
   
     //暂时不做任何异常处理
     async index(){
       // let id = this.ctx.request.query.id;
       // let clazzList = await this.app.model.Clazz.findAll({
       //   where:{
       //     id:id
       //   }
       // });
       let clazzList = await this.app.model.Clazz.findAll();
       this.ctx.body = clazzList;
     }
   
     async create(){
       let name = this.ctx.request.body.name;
       await this.app.model.Clazz.create({
         name:name
       })
       this.ctx.body = '添加成功';
     }
   
     async destroy(){
       let id = this.ctx.params.id;
       await this.app.model.Clazz.destroy({where:{
         id:id
       }})
       this.ctx.body = '删除数据';
     }
   
     async update(){
       let id = this.ctx.params.id;
       let name = this.ctx.request.body.name;
       await this.app.model.Clazz.update({name:name},{
         where:{
           id:id
         }
       })
   
       this.ctx.body = '修改成功';
     }
   
   }
   module.exports = ClazzController;
   
   
   
   
   
   //在postman 进行 get put delete post 请求可调用响应方法
   ````

9. 如何添加外键

   ````javascript
   //在model文件夹下新建student.js模型,
   module.exports = app => {
     const {STRING,DOUBLE} = app.Sequelize;
     const Student = app.model.define('student',{
       name: STRING,
       achievement: DOUBLE
     })
   
     return Student;
   }
   //在这个模型中添加如下代码，新增外键
   module.exports = app => {
     const {STRING,DOUBLE} = app.Sequelize;
     const Student = app.model.define('student',{
       name: STRING,
       achievement: DOUBLE
     })
     
     Student.associate = function(){ //所属哪个班级，指向班级的主键
         app.model.Student.belongsTo(app.model.Clazz,{
             foreignKey:'clazz_id',
             as:'clazz'
         })
     }
   
     return Student;
   }
   
   ````

10. 在controller中新建student.js

    ````javascript
    const Controller = require('egg').Controller;
    
    class StudentController extends Controller {
    	
    }
    
    module.exports = StudentController;
    ````

11. 在router.js中注册（这里用restful风格路由）

    ````javascript
    router.resources('student','/student',controller.student);
    ````

    

12. 然后在controller中的student.js 可以实现增删改查

    ````javascript
    const Controller = require('egg').Controller;
    
    class StudentController extends Controller{
      async create(){ //增加
        let name = this.ctx.request.body.name;
        let achievement = this.ctx.request.body.achievement;
        let clazz_id = this.ctx.request.body.clazz_id;
        await this.app.model.Student.create({
          name:name,
          achievement:achievement,
          clazz_id:clazz_id
        })
        this.ctx.body = '添加成功';
      }
    
      async index(){ //查看
        let studentList = await this.app.model.Student.findAll();
    
        this.ctx.body = studentList;
      }
    }
    
    module.exports = StudentController;
    ````

    

### Service

更加容易维护，减小controller的体积大小。把数据库交互方法交给service，可以让controller多次调用。

在app/service 目录下创建xxx.js文件，定义方法与定义controller类似

可以在service中处理异常

````javascript
//   app/service/student.js
const Service = require('egg').Service;

class StudentService extends Service {
    //查询
     async getStudentList(){
         try{
             let studentList = await this.app.model.Student.findAll();
             return studentList;
         }catch(e){
             return null;
         }
     }
    
    //增加
    async createStudent (name,achievement,clazz_id){
        try{
          await this.app.model.Student.create({
            name:name,
            achievement:achievement,
            clazz_id:clazz_id
          })
          return true;
        }catch(e){
          return false;
        }
  }
}

module.exports = StudentService;
````

在controller/student.js 调用为

````javascript
......
async index(){
    let list = await this.ctx.service.student.getStudentList();
    if(list){
      this.ctx.body = {
        code:20000,
        data:list
      }
    }else{
      this.ctx.body = {
        code:50000,
        msg:'服务器异常'
      }
    }
  }

async create(){
    let name = this.ctx.request.body.name;
    let achievement = this.ctx.request.body.achievement;
    let clazz_id = this.ctx.request.body.clazz_id;
    let result = await this.ctx.service.student.createStudent(name,achievement,clazz_id);
    if(result){
      this.ctx.body = {
        code:20000,
        msg:"成功"
      }
    }else{
      this.ctx.body = {
        code:50000,
        mag:"失败"
      }
    }
  }
......
````

用postman查看请求结果，证明调用没问题。



## 基于 Egg 和 Vue 的项目

1. 新建数据库 与egg下面 sequelize 要对应

2. 新建一个egg 项目 和 vue项目(egg项目搭建上面已经有了)

3. 安装必要的第三方依赖： egg:  egg-sequelize  egg-cors  egg-jwt  vue:   axios

4. 引入和配置插件（plugin.js   +   config.default.js）

5. 编写数据模型(在app 新建 model 文件夹，新建 hello.js**(什么名字都可)**，存放数据模型)

   ````javascript
   module.exports = app =>{
       const {STRING} = app.Sequelize;
       const Hello = app.model.define('hello',{
           msg:STRING,
       })
       return Hello;
   }
   ````

6. 创建表，在 根目录下 新建 app.js 

   ````javascript
   module.exports = app => {
       //beforeStart egg 生命周期函数 启动应用时执行
       app.beforeStart(async function (){
           //创建数据表
           await app.model.sync({})
       })
   }
   
   //控制台已经创建好这个表
   ````

7.  在 可视化SQL 工具中 给hellos 表新建一条数据

8. 在router.js中 注册controller

   ````javascript
   ...
   router.get('/hello',controller.hello.index);
   router.post('/login',controller.login.login);
   ...
   ````

9. 在 controller 下 新建 hello.js    login.js

   ````javascript
   //hello.js
   const Controller = require('egg').Controller;
   
   class HelloController extends Controller {
       async index(){
           //调用service里面的方法
           let result = await this.ctx.service.hello.getMessage();
           if(result){
              this.ctx.body = {
                  code:20000,
                  data:result
              }
           }else{
               this.ctx.body = {
                  code:50000,
                  data:'数据获取失败'
              }
           }
       }
   }
   
   module.exports = HelloController;
   
   
   //写完后用 postman 测试  get 测试无误 有返回值
   
   
   //login.js 
   const Controller = require('egg').Controller;
   
   class LoginController extends Controller {
       //拿到用户名密码等  给前端回调token
       async login (){
           try{
               let username = this.ctx.request.body.username;
               let token = this.ctx.app.jwt({
                   username
               },this.ctx.app.config.jwt.sercet);
               this.ctx.body = {
                   code:20000,
                   token
               }
           }catch(e){
               this.ctx.body = {
                   code:40000,
                   msg:'用户名或密码错误'
               }
           }
          
       }
   }
   
   module.exports = LoginController;
   
   
   //写完后用postman 测试 post 无误 有返回值
   ````

10. 在app 下 新建service文件夹  新建hello.js **(只新建了获取数据的service)**

    ````javascript
    const Service = require('egg').Service;
    
    class HelloService extends Service {
        async getMessage() {
            try{
                //调用模型中的方法
                return await this.app.model.Hello.findAll();
            }catch(e){
                return null;
            }
        }
    }
    
    module.exports = HelloService;
    ````

11. token验证   在app 下 新建 middleware 文件夹 新建 checktoken.js 文件

    ````javascript
    function checktoken() {
        return async function (ctx,next){
            try{
                 //验证token
                let token = ctx.header.token;
                ctx.app.jwt.verify(token,ctx.app.config.sercet);
                await next();
            }catch(e){
                ctx.body = {
                    code:40000,
                    msg:'登录失败'
                }
            }
           
        }
    }
    module.exports = checktoken;
    ````

12. 在router.js 中 给需要中间件的路由添加中间件

    ````javascript
    //router.js
    
    router.get('/hello',app.middleware.checktoken,controller.hello.index);
    
    //添加完成 用postman 测试 无误 能有不同情况 (有无token)
    ````

    至此 后端简单设置结束

13. **前端设置**

    页面 + js       登录 + 获取数据

14. vue项目打包好以后吧dist文件夹下的 内容 复制到 egg 项目下的 app / public中, egg服务运行，输入127.0.0.1/public/index.html，会显示404，进行下一步配置

15. 在config.default.js中写入 

    ````javascript
    //先引入path
    const path = require("path");
    
    
    
    ...
    config.static = {
        prefix: '/', //访问路径
        dir: path.join(appInfo.baseDir,"app/public"),//设置静态文件目录
    };
    ...
    ````

16. 直接访问 http://127.0.0.1:7001/index.html  项目部署完成

17. egg 终端中 启动服务 npm start  终止服务  npm stop

