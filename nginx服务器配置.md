[TOC] 

# 一、nginx介绍

> - 开源 轻量级 高性能 静态资源web服务器
> - 同时也是优秀的反向代理服务器、缓存服务器、邮件服务器
> - Apache：稳定 庞大 多模块化
> - nginx：轻量（占用内存少） 并发能力强

> - 在高连接、高并发情况下，nginx是Apache的很不错的替代品。
>
> - 理论上支持50000并发。
>
> - 使用异步非阻塞的epoll模型，内存消耗很少。
> - 配置文件非常简单  实现差异化简单
> - Rewrite 重写规则非常强大 支持正则
> - 内置健康检查功能 -> (后端web服务器宕机，不影响前端访问)
> - 节省带宽 -> 支持gzip压缩 (可定义压缩级别)，可以添加浏览器本地缓存
> - 稳定性高 -> 用于反向代理，宕机几率低
> - 模块化设计：模块可以动态编译
> - 热部署： 不停机重载配置文件（可以进行平滑升级），性能好。
> - 成本低廉(尤其是作为负载均衡)

# 二、nginx基本安装

1. 使用yum进行安装

   > nginx在普通的本地yum仓库中没有rpm程序包，需要基于epel源进行安装
   >
   > 安装：` yum -y install nginx `
   >
   > 查看目录：`rpm -ql nginx`

2. 配置文件

   > - 主配置文件：`vim /etc/nginx/nginx.conf`
   >
   > ![image-20191119102053779](/Users/celeven/Library/Application Support/typora-user-images/image-20191119102053779.png)
   >
   > ![image-20191119102119822](/Users/celeven/Library/Application Support/typora-user-images/image-20191119102119822.png)

3. Nginx 命令

   > 启动：`systemctl start nginx`
   >
   > 监听：`ss -tnl`
   >
   > 网页根路径：`/usr/share/nginx/html/`

