css层级较高，遮住下面的元素。而下面的元素还有点击事件的时候，无需改动css样式 只要给层级较高的元素添加一个css属性，那么它将不会捕捉到任何点击事件。http://caibaojian.com/pointer-events.html

````javascript
<style>
	.overlay {
		pointer-events: none;
	}
</style>
	
<div id="overlay" class="overlay"></div>
````



#### 浏览器支持：

可以看canIUse上的数据，目前支持IE11、Firefox3.6+、Chrome、Safari4+、Opera15+、Android、IOS.