## 简单Base64原理

````javascript
var str1 = new Array(26).fill(65).map((item,index) =>String.fromCodePoint(item + index)).join('')
var str2 = new Array(26).fill(97).map((item,index) =>String.fromCodePoint(item + index)).join('')
var base64Code = str1 + str2 + '0123456789+/'
console.log(base64Code.length)
function toBase64(str){
  var item = '',
      encode = '',
      res = '';
  var code =  str.split('')
              .reduce((prev,curr) => prev + curr.charCodeAt().toString(2).padStart(8,'0'),'')

  if(code.length % 24 === 8){
    code += '0000'
    res += '=='
  }
  if(code.length % 24 === 16){
    code += '00'
    res += '='
  }
  
  for(var i = 0; i < code.length; i+=6){
    item = code.slice(i,i+6)
    console.log(item)
    encode += base64Code[parseInt(item,2)]
  }
  return encode + res
}

console.log(toBase64('js++'))
````

