## 一、JS字符串String常用方法

1. charAt()     返回指定位置的字符。

```javascript
str.charAt(index)  
index 为必须参数，类型为number（0到str.length-1之间，否则该方法返回 空串）
另外：str.charAt()即不带参数和str.charAt(NaN)均返回字符串的第一个字符
```

2. charCodeAt()    返回在指定的位置的字符的 Unicode 编码。

```
str.charCodeAt(index)  
index 为必须参数，类型为number（0到str.length-1之间，否则该方法返回 NaN）
```

3. concat()   用于连接两个或多个字符串。　　

```
var a = "hello",b = "kitty",c = "!";
a.concat(b,c) // 功能和 “+” 拼接没啥两样  
```

4. fromCharCode()    接受一个指定的 Unicode 值，然后返回一个字符串。

```
String.fromCharCode(unicode1,unicode2,...,nuicodeX) 该方法是 String 的静态方法，语法应该是 String.fromCharCode()。
```

5. indexOf() 方法      返回指定字符串在字符串中**首次出现的位置**。匹配不到则返回-1。

```
str.indexOf(searchStr,startIndex)  
searchStr必选，表示需要匹配的字符串值；
startIndex可选，取值范围0到str.length-1，省略则默认首字符开始检索。
```

6. lastIndexOf()      返回指定字符串值**最后出现的位置**，在一个字符串中的指定位置从后向前搜索。

```
str.lastIndexOf(searchStr,startIndex)  
searchStr必选，表示需要匹配的字符串值；
startIndex可选，取值范围0到str.length-1，省略则默认尾字符开始检索。
```

7.  match()        在字符串内检索指定的值，或找到一个或多个正则表达式的匹配。匹配不到返回Null。

```
str.match(regExp)  
```

![img](https://images2017.cnblogs.com/blog/1128668/201708/1128668-20170817000829646-2001057811.png)

![img](https://images2017.cnblogs.com/blog/1128668/201708/1128668-20170817000901287-1983442819.png)

8.  replace()  用于在字符串中用一些字符替换另一些字符，或替换一个与正则表达式匹配的子串。

```
str.replace(regexp/substrOld,replaceStrNew)  
```

![img](https://images2017.cnblogs.com/blog/1128668/201708/1128668-20170817003544256-951030466.png)

9.  search()  用于检索字符串中指定的子字符串，或检索与正则表达式相匹配的子字符串。无匹配返回-1。

```
str.search(regexp/substr)  返回值：str中第一个与正则或字符串相匹配的子串的起始位置。

说明 search() 方法不执行全局匹配，它将忽略标志 g。它同时忽略 regexp 的 lastIndex 属性，并且总是从字符串的开始进行检索，这意味着它总是返回 stringObject 的第一个匹配的位置。
```

10.  slice()   提取字符串的某个部分，并以新的字符串返回被提取的部分。

```
str.slice(startIndex,endIndex)  返回值包含startIndex不包含endIndex
忽略endIndex则返回包括startIndex到原字符串结尾的字符串
另外参数还有负数反向用法
```

11.  split()  用于把一个字符串分割成字符串数组。

```
str.split()  
```

12.  substr() 方法可在字符串中抽取从 *start* 下标开始的指定数目的字符。

```
str.substr(startIndex,length)  忽略length则返回从startIndex到字符串尾字符
```

13. substring() 方法用于提取字符串中介于两个指定下标之间的字符。

```
str.substring(startIndex,endIndex)  忽略endIndex则返回从startIndex到字符串尾字符
```

14. toLocaleUpperCase() / toLocaleLowerCase()  用于字符串转换大小写（与下面的方法方法仅在某些外国小语种有差别）

15. toUpperCase() / toLowerCase()  用于字符串转换大小写

## 二、JS数组的常用方法

1. join

   就是把数组转换成字符串，然后给他规定个连接字符，默认的是逗号( ，)

　　书写格式：join(" ")，括号里面写字符串 ("要加引号"),

```
var arr = [1,2,3]; 
console.log(arr.join()); 　　　　// 1,2,3 
console.log(arr.join("-")); 　　// 1-2-3 
console.log(arr); 　　　　　　　　// [1, 2, 3]（原数组不变）
```

 

2. push()和pop()

   push(): 把里面的内容添加到数组末尾，并返回修改后的长度。

   pop()：移除数组最后一项，返回移除的那个值，减少数组的length。

   　　　　书写格式：arr.push(" ")，括号里面写内容 ("字符串要加引号"),

   　　　  书写格式：arr.pop( )

```
var arr = ["Lily","lucy","Tom"]; 
var count = arr.push("Jack","Sean"); 
console.log(count); 　　　　　　　　　　// 5 
console.log(arr); 　　　　　　　　　　　// ["Lily", "lucy", "Tom", "Jack", "Sean"] var item = arr.pop(); 
console.log(item); 　　　　　　　　　　 // Sean 
console.log(arr); 　　　　　　　　　　  // ["Lily", "lucy", "Tom", "Jack"]
```

3. shift() 和 unshift()  (和上面的push，pop相反，针对第一项内容)

   **shift()：删除原数组第一项，并返回删除元素的值；如果数组为空则返回undefined 。 **

   **unshift:将参数添加到原数组开头，并返回数组的长度 。**

   　　　　书写格式：arr.shift(" ")，括号里面写内容 ("字符串要加引号"),

```
var arr = ["Lily","lucy","Tom"]; 
var count = arr.unshift("Jack","Sean"); 
console.log(count); 　　　　　// 5 
console.log(arr);　　　　　　//["Jack", "Sean", "Lily", "lucy", "Tom"] 
var item = arr.shift(); 
console.log(item); 　　　　　// Jack 
console.log(arr); 　　　　 // ["Sean", "Lily", "lucy", "Tom"]
```

4. sort()

   **sort()：将数组里的项从小到大排序**

   　　　  书写格式：arr.sort( )

 

```
var arr1 = ["a", "d", "c", "b"]; 
console.log(arr1.sort()); 　　　　　　　　　　// ["a", "b", "c", "d"]
```

sort()方法比较的是字符串，没有按照数值的大小对数字进行排序，要实现这一点，就必须使用一个排序函数

```
function sortNumber(a,b) 
{
	return a - b
 }
 
 arr = [13, 24, 51, 3]; 
 console.log(arr.sort()); 　　　　　　　　　　// [13, 24, 3, 51] console.log(arr.sort(sortNumber)); 　　　　// [3, 13, 24, 51](数组被改变)
```

5. reverse()

   reverse()：反转数组项的顺序。

   　　　  书写格式：arr.reverse( )

```
var arr = [13, 24, 51, 3]; 
console.log(arr.reverse()); 　　　　　　　　//[3, 51, 24, 13] 
console.log(arr); 　　　　　　　　　　　　　　//[3, 51, 24, 13](原数组改变)
```

6. concat()

   **concat() ：将参数添加到原数组中。这个方法会先创建当前数组一个副本，然后将接收到的参数添加到这个副本的末尾，最后返回新构建的数组。在没有给 concat()方法传递参数的情况下，它只是复制当前数组并返回副本。**

   　　　　书写格式：arr.concat()，括号里面写内容 ("字符串要加引号"),

```
var arr = [1,3,5,7]; 
var arrCopy = arr.concat(9,[11,13]); 
console.log(arrCopy); 　　　　　　　　　　　　//[1, 3, 5, 7, 9, 11, 13] console.log(arr); 　　　　　　　　　　　　　　// [1, 3, 5, 7](原数组未被修改)
```

7. slice()

   **slice()：返回从原数组中指定开始下标到结束下标之间的项组成的新数组。slice()方法可以接受一或两个参数，即要返回项的起始和结束位置。在只有一个参数的情况下， slice()方法返回从该参数指定位置开始到当前数组末尾的所有项。如果有两个参数，该方法返回起始和结束位置之间的项——但不包括结束位置的项。**

   　　　　书写格式：arr.slice( 1 , 3 )

```
var arr = [1,3,5,7,9,11]; 
var arrCopy = arr.slice(1); 
var arrCopy2 = arr.slice(1,4); 
var arrCopy3 = arr.slice(1,-2); 
var arrCopy4 = arr.slice(-4,-1); 
console.log(arr); 　　　　　　　　　　　　　　//[1, 3, 5, 7, 9, 11](原数组没变) console.log(arrCopy); 　　　　　　　　　　　 //[3, 5, 7, 9, 11] console.log(arrCopy2); 　　　　　　　　　　　//[3, 5, 7] 
console.log(arrCopy3); 　　　　　　　　　　　//[3, 5, 7] 
console.log(arrCopy4); 　　　　　　　　　　　//[5, 7, 9]
```

　　arrCopy只设置了一个参数，也就是起始下标为1，所以返回的数组为下标1（包括下标1）开始到数组最后。 

　　arrCopy2设置了两个参数，返回起始下标（包括1）开始到终止下标（不包括4）的子数组。 

　　arrCopy3设置了两个参数，终止下标为负数，当出现负数时，将负数加上数组长度的值（6）来替换该位置的数，因此就是从1开始到4（不包括）的子数组。 

　　arrCopy4中两个参数都是负数，所以都加上数组长度6转换成正数，因此相当于slice(2,5)。

8. splice()

   splice()：删除、插入和替换。

   删除：指定 2 个参数：要删除的第一项的位置和要删除的项数。

   　　　　书写格式：arr.splice( 1 , 3 )

    

   插入：可以向指定位置插入任意数量的项，只需提供 3 个参数：起始位置、 0（要删除的项数）和要插入的项。

   　　　　书写格式：arr.splice( 2,0,4,6 )
   替换：可以向指定位置插入任意数量的项，且同时删除任意数量的项，只需指定 3 个参数：起始位置、要删除的项数和要插入的任意数量的项。插入的项数不必与删除的项数相等。

   　　　　书写格式：arr.splice( 2,0,4,6 )

```
var arr = [1,3,5,7,9,11]; 
var arrRemoved = arr.splice(0,2); 
console.log(arr); 　　　　　　　　　　　　　　　//[5, 7, 9, 11] console.log(arrRemoved); 　　　　　　　　　　　//[1, 3] 
var arrRemoved2 = arr.splice(2,0,4,6); 
console.log(arr); 　　　　　　　　　　　　　　　// [5, 7, 4, 6, 9, 11] console.log(arrRemoved2); 　　　　　　　　　　// [] 
var arrRemoved3 = arr.splice(1,1,2,4); 
console.log(arr); 　　　　　　　　　　　　　　　// [5, 2, 4, 4, 6, 9, 11] console.log(arrRemoved3); 　　　　　　　　　　//[7]
```

9. indexOf()和 lastIndexOf()

   **indexOf()：接收两个参数：要查找的项和（可选的）表示查找起点位置的索引。其中， 从数组的开头（位置 0）开始向后查找。**

   　　　　书写格式：arr.indexof( 5 )

   **lastIndexOf：接收两个参数：要查找的项和（可选的）表示查找起点位置的索引。其中， 从数组的末尾开始向前查找**

   　　　　书写格式：arr.lastIndexOf( 5,4 )

```
var arr = [1,3,5,7,7,5,3,1];  
console.log(arr.indexOf(5)); 　　　　　　//2 
console.log(arr.lastIndexOf(5)); 　　　 //5 
console.log(arr.indexOf(5,2)); 　　　　 //2 
console.log(arr.lastIndexOf(5,4)); 　　//2 
console.log(arr.indexOf("5")); 　　　　 //-1
```

10. forEach()

    **forEach()：对数组进行遍历循环，对数组中的每一项运行给定函数。这个方法没有返回值。参数都是function类型，默认有传参，参数分别为：遍历的数组内容；第对应的数组索引，数组本身。**

    　　　　书写格式：arr.forEach()

```
var arr = [1, 2, 3, 4, 5]; 
`arr.forEach(function(x, index, a){ console.log(x + '|' + index + '|' + (a === arr)); }); 
// 输出为： 
// 1|0|true 
// 2|1|true 
// 3|2|true 
// 4|3|true 
// 5|4|true
```

11. map()

    **map()：指“映射”，对数组中的每一项运行给定函数，返回每次函数调用的结果组成的数组。**

    　　　　书写格式：arr.map()

```
var arr = [1, 2, 3, 4, 5]; 
var arr2 = arr.map(function(item){ return item*item; }); 
console.log(arr2); 　　　　　　　　//[1, 4, 9, 16, 25]
```

12. filter()

    **filter()：“过滤”功能，数组中的每一项运行给定函数，返回满足过滤条件组成的数组。**

    　　　　书写格式：arr.filter()

```
var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]; 
var arr2 = arr.filter(function(x, index) { return index % 3 === 0 || x >= 8; });  console.log(arr2); 　　　　　　　　//[1, 4, 7, 8, 9, 10]
```

13. every()

    **every()：判断数组中每一项都是否满足条件，只有所有项都满足条件，才会返回true。**

    　　　　书写格式：arr.every()

```
	var arr = [1, 2, 3, 4, 5]; 
	var arr2 = arr.every(function(x) { return x < 10; });  
	console.log(arr2); 　　　　　　　　//true 
	var arr3 = arr.every(function(x) { return x < 3; });  
	console.log(arr3); 　　　　　　　　// false
```

14. some()

    **some()：判断数组中是否存在满足条件的项，只要有一项满足条件，就会返回true。**

    　　　　**书写格式：arr.some()**

```javascript
var arr = [1, 2, 3, 4, 5]; 
var arr2 = arr.some(function(x) { return x < 3; });  
console.log(arr2); 　　　　　　　　//true 
var arr3 = arr.some(function(x) { return x < 1; });  
console.log(arr3); 　　　　　　　　// false
```

15. reduce()

    ​    遍历数组，调用回调函数，将数组元素组合成一个值，reduce从索引最小值开始，reduceRight反向，方法有两个参数
    1. 回调函数，把两个值合成一个，返回结果
    2. value,一个初始值，可选

```javascript
var arr = [1,2,3,4,5,6]
console.log(arr.reduce(function(v1,v2){
    return v1 + v2;
})) // 21
console.log(arr.reduce((pre,cur)=>pre+cur,0))  //21  简写形式
console.log(arr.reduce((pre,cur)=>{
   return pre+cur 
},0)) //21   不简写形式
//开始是1+2 = 3，之后3+3 =6，之后6+4 =10，之后
//10+5 =15，最后15+6 =21
console.log(arr.reduce(function(v1,v2){
    return v1 - v2;
},100)) // 79
//开始的时候100-1=99，之后99-2=97，之后97-3=94，之后
//94-4=90，之后90-5=85，最后85-6=79
```

## 三、JS数组转对象

1. for ... in...

       ```javascript
   let obj = new Object()
   let arr = [1,2,3,4,5]
   for(let i in arr){
       obj[i] = arr[i]
   }
   console.log(obj) // {0: 1, 1: 2, 2: 3, 3: 4, 4: 5}
       ```

   

2. Object.assign()

   ```javascript
   let obj = new Object()
   let arr = [1,2,3,4,5]
   let result = Object.assign(obj,arr)
   console.log(result) // {0: 1, 1: 2, 2: 3, 3: 4, 4: 5}
   ```

   

3.  ...展开运算符

   ```javascript
   let a = [1,2,3,4,5]
   
   console.log({...a})    // {0: 1, 1: 2, 2: 3, 3: 4, 4: 5}
   ```

   

4.  map

   ```javascript
   var a = [1,2,3,4,5]
   var b = {}
   var c = a.map((v,i)=>{
       return b[i] = v
   })
   console.log(c) // {0: 1, 1: 2, 2: 3, 3: 4, 4: 5}
   ```

   

5. forEach

   ```javascript
   var a = [1,2,3,4,5]
   var b = {}
   var c = a.forEach((v,i)=>obj[i] = v)
   console.log(c) // {0: 1, 1: 2, 2: 3, 3: 4, 4: 5}
   
   
   //forEach 中的跳过
   let a = [1,2,3,4,5]
   let b = {}
   a.forEach((v,i)=>{
       if(v == 2){
           return
       }
       b[i] = v
   })
   console.log(b) // {0: 1, 2: 3, 3: 4, 4: 5}
   ```

   

6. for

   ```javascript
   let a = [1,2,3,4,5]
   let b = {}
   for(var i = 0; i < a.length;i++){
       b[i] = a[i]
   }
   console.log(b)  // {0: 1, 2: 3, 3: 4, 4: 5}
   ```

   

## 四、是否改变原数组的常用方法归纳

**改变原数组的：**

1. shift：将**第一个元素**删除并且返回删除元素，空即为undefined
2. unshift：向数组**开头添加**元素，并返回新的长度
3. pop：删除**最后一个**并返回删除的元素
4. push：向数组**末尾添加**元素，并返回新的长度
5. reverse：颠倒数组顺序
6. sort：对数组排序
7. splice:splice(start,length,item)删，增，替换数组元素，返回被删除数组，无删除则不返回

**不改变原数组的：**

1. concat：连接多个数组，返回新的数组
2. join：将数组中所有元素以参数作为分隔符放入一个字符
3. slice：slice(start,end)，返回选定元素
4. map,filter,some,every等不改变原数组

**splice和slice的区别：**
splice(i,j,”a”) 删除，添加元素，splice() 方法与 slice() 方法的作用是不同的，splice() 方法会直接对数组进行修改。从i开始删j个(包括i),并将”a”插入到i处。
slice(start,end) 从某个已有的数组返回选定的元素，从start位开始返回到end（包括start不包括end）如果是负数，表示从数组尾部进行计算（同样：包括start不包括end）,请注意，该方法并不会修改数组，而是返回一个子数组。

![这里写图片描述](https://img-blog.csdn.net/20170909195508215?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvY3Jpc3RpbmFfc29uZw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

![这里写图片描述](https://img-blog.csdn.net/20170909195610535?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvY3Jpc3RpbmFfc29uZw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
**splice()的强大(删、增、替换数组的元素)**

```
<span style="font-size:18px;">处理数组的方法很多，javascript splice()算是最强大的了，它可以用于插入、删除或替换数组的元素。下面来一一介绍！   
  
1.删除-用于删除元素，两个参数，第一个参数（要删除第一项的位置），第二个参数（要删除的项数）   
2.插入-向数组指定位置插入任意项元素。三个参数，第一个参数（其实位置），第二个参数（0），第三个参数（插入的项）   
3.替换-向数组指定位置插入任意项元素，同时删除任意数量的项，三个参数。第一个参数（起始位置），第二个参数（删除的项数），第三个参数（插入任意数量的项）   
  
<strong>看下面这段代码就明白了</strong>   
  
</span>  
123456789
<span style="font-size:18px;">var lang = ["php","java","javascript"];   
//删除   
var removed = lang.splice(1,1);   
alert(lang); //php,javascript   
alert(removed); //java ,返回删除的项   
//插入   
var insert = lang.splice(0,0,"asp"); //从第0个位置开始插入   
alert(insert); //返回[]   
alert(lang); //["asp", "php", "java", "javascript"]   
//替换   
var replace = lang.splice(1,1,"c#","ruby"); //删除一项，插入两项   
alert(lang); //asp,c#,ruby ，javascript  
alert(replace); //php,返回删除的项 </span>  
```