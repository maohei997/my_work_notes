## 一、let 和 const

ES6 新增了`let`命令，用来声明变量。它的用法类似于`var`，但是所声明的变量，只在`let`命令所在的代码块内有效。

### 作用域

```javascript
{
    var a = 10
    let b = 1
}
console.log(a) //10
console.log(b) //b is not defined
```

计数器中 `let` 和 `var`  的不同

```javascript
var a = []

for(var i = 0; i < 10; i++){
    a[i] = function(){
        console.log(i)
    }
}
a[0]()   //10

//上面代码中，变量i是var命令声明的，在全局范围内都有效，所以全局只有一个变量i。每一次循环，变量i的值都会发生改变，而循环内被赋给数组a的函数内部的console.log(i)，里面的i指向的就是全局的i。也就是说，所有数组a的成员里面的i，指向的都是同一个i，导致运行时输出的是最后一轮的i的值，也就是 10。

//====================================================

var a = []

for(let i = 0; i < 10; i++){
    a[i] = function(){
        console.log(i)
    }
}
a[0]()  //0

//上面代码中，变量i是let声明的，当前的i只在本轮循环有效，所以每一次循环的i其实都是一个新的变量，所以最后输出的是6。你可能会问，如果每一轮循环的变量i都是重新声明的，那它怎么知道上一轮循环的值，从而计算出本轮循环的值？这是因为 JavaScript 引擎内部会记住上一轮循环的值，初始化本轮的变量i时，就在上一轮循环的基础上进行计算。
```

另外，`for`循环还有一个特别之处，就是设置循环变量的那部分是一个父作用域，而循环体内部是一个单独的子作用域。

```javascript
for(let i = 0; i < 2; i++){
   let i = 'aaaa'
   console.log(i)
}
//aaaa
//aaaa
//aaaa
```

上面代码正确运行，输出了 3 次`abc`。这表明函数内部的变量`i`与循环变量`i`不在同一个作用域，有各自单独的作用域。

### 不存在变量提升

`var`命令会发生“变量提升”现象，即变量可以在声明之前使用，值为`undefined`。这种现象多多少少是有些奇怪的，按照一般的逻辑，变量应该在声明语句之后才可以使用。

为了纠正这种现象，`let`命令改变了语法行为，它所声明的变量一定要在声明后使用，否则报错。

```javascript
//var
console.log(foo) //输出undefined
var foo

//let
console.log(foo)//报错ReferenceError
let foo
```

上面代码中，变量`foo`用`var`命令声明，会发生变量提升，即脚本开始运行时，变量`foo`已经存在了，但是没有值，所以会输出`undefined`。变量`bar`用`let`命令声明，不会发生变量提升。这表示在声明它之前，变量`bar`是不存在的，这时如果用到它，就会抛出一个错误。

### 暂时性死区

只要块级作用域内存在`let`命令，它所声明的变量就“绑定”（binding）这个区域，不再受外部的影响。

```javascript
var tmp = 123;
if(true){
    tmp = 'aaa' //报错ReferenceError
    let tmp
}
```

上面代码中，存在全局变量`tmp`，但是块级作用域内`let`又声明了一个局部变量`tmp`，导致后者绑定这个块级作用域，所以在`let`声明变量前，对`tmp`赋值会报错。

ES6 明确规定，如果区块中存在`let`和`const`命令，这个区块对这些命令声明的变量，从一开始就形成了封闭作用域。凡是在声明之前就使用这些变量，就会报错。

总之，在代码块内，使用`let`命令声明变量之前，该变量都是不可用的。这在语法上，称为“暂时性死区”（temporal dead zone，简称 TDZ）。

```javascript
if(true){
    //TDZ开始
    tmp = 'abc'
    console.log(tmp) //ReferenceError
    //TDZ结束
    let tmp
    console.log(tmp)//undefined
    
    tmp = 112
    console.log(tmp)//112
}
```

上面代码中，在`let`命令声明变量`tmp`之前，都属于变量`tmp`的“死区”。

“暂时性死区”也意味着`typeof`不再是一个百分之百安全的操作

```javascript
typeof x  //ReferenceError
let x 
```

上面代码中，变量`x`使用`let`命令声明，所以在声明之前，都属于`x`的“死区”，只要用到该变量就会报错。因此，`typeof`运行时就会抛出一个`ReferenceError`。

作为比较，如果一个变量根本没有被声明，使用`typeof`反而不会报错。

```javascript
typeof dwadadadadaw // "undefined"
```

上面代码中，`undeclared_variable`是一个不存在的变量名，结果返回“undefined”。所以，在没有`let`之前，`typeof`运算符是百分之百安全的，永远不会报错。现在这一点不成立了。

某些隐蔽 “ 死区 ” 

```javascript
function bar(x=y,y=2){
    return [x,y]
}
console.log(bar())  //ReferenceError
```

上面代码中，调用`bar`函数之所以报错（某些实现可能不报错），是因为参数`x`默认值等于另一个参数`y`，而此时`y`还没有声明，属于“死区”。如果`y`的默认值是`x`，就不会报错，因为此时`x`已经声明了。

```javascript
function bar(x = 2, y = x) {
  return [x, y];
}
console.log(bar()) // [2, 2]
```

另外，下面的代码也会报错，与`var`的行为不同。

```javascript
// 不报错
var x = x;

// 报错
let x = x;
// ReferenceError: Cannot access 'x' before initialization  初始化前无法访问“x”
```

上面代码报错，也是因为暂时性死区。使用`let`声明变量时，只要变量在还没有声明完成前使用，就会报错。上面这行就属于这个情况，在变量`x`的声明语句还没有执行完成前，就去取`x`的值，导致报错”x 未定义“。

ES6 规定暂时性死区和`let`、`const`语句不出现变量提升，主要是为了减少运行时错误，防止在变量声明前就使用这个变量，从而导致意料之外的行为。

总之，暂时性死区的本质就是，**只要一进入当前作用域，所要使用的变量就已经存在了，但是不可获取，只有等到声明变量的那一行代码出现，才可以获取和使用该变量**

### 不允许重复声明

`let`不允许在相同作用域内，重复声明同一个变量。

```javascript
// 报错
function func() {
  let a = 10;
  var a = 1;
}
func()  //SyntaxError: Identifier 'a' has already been declared 标识符“a”已声明


// 报错
function func() {
  let a = 10;
  let a = 1;
}
func()  //SyntaxError: Identifier 'a' has already been declared 标识符“a”已声明
```

因此，不能在函数内部重新声明参数。

```javascript
function func(arg) {
  let arg;
}
func() // 报错

function func(arg) {
  {
    let arg;
  }
}
func() // 不报错  新的子作用域
```

### 块级作用域

ES5 只有全局作用域和函数作用域，没有块级作用域，这带来很多不合理的场景。

第一种场景，内层变量可能会覆盖外层变量。

```javascript
var tmp = new Date();

function f() {
  console.log(tmp);
  if (false) {
    var tmp = 'hello world';
  }
}

f(); // undefined   变量提升  不受if作用域限制
```

上面代码的原意是，`if`代码块的外部使用外层的`tmp`变量，内部使用内层的`tmp`变量。但是，函数`f`执行后，输出结果为`undefined`，原因在于变量提升，导致内层的`tmp`变量覆盖了外层的`tmp`变量。

第二种场景，用来计数的循环变量泄露为全局变量。

```javascript
var s = 'hello';

for (var i = 0; i < s.length; i++) {
  console.log(s[i]);
}

console.log(i); // 5
```

上面代码中，变量`i`只用来控制循环，但是循环结束后，它并没有消失，泄露成了全局变量。



改造一下

```javascript
var s = 'aaaaa'

for(let i = 0; i < s.length; i++){
    console.log(s[i])
}
console.log(i) //ReferenceError: i is not defined
```

### ES6的块级作用域

`let`实际上为 JavaScript 新增了块级作用域。

```javascript
function f1() {
  let n = 5;
  if (true) {
    let n = 10;
  }
  console.log(n); // 5  let 不会声明提升
}
```

上面的函数有两个代码块，都声明了变量`n`，运行后输出 5。这表示外层代码块不受内层代码块的影响。如果两次都使用`var`定义变量`n`，最后输出的值才是 10。

ES6 允许块级作用域的任意嵌套。

```javascript
{{{{
  {let insane = 'Hello World'}
  console.log(insane); // 报错 ReferenceError: insane is not defined
}}}};
```

上面代码使用了一个五层的块级作用域，每一层都是一个单独的作用域。第四层作用域无法读取第五层作用域的内部变量。

内层作用域可以定义外层作用域的同名变量。

```javascript
{{{{
  let insane = 'Hello World';
  {let insane = 'Hello World'}
}}}};
```

块级作用域的出现，实际上使得获得广泛应用的匿名立即执行函数表达式（匿名 IIFE）不再必要了。

```javascript
// IIFE 写法
(function () {
  var tmp = ...;
  ...
}());

// 块级作用域写法
{
  let tmp = ...;
  ...
}
```

### 块级作用域与函数声明

ES5 规定，函数只能在顶层作用域和函数作用域之中声明，不能在块级作用域声明。

```javascript
// 情况一
if (true) {
  function f() {}
}

// 情况二
try {
  function f() {}
} catch(e) {
  // ...
}
```

上面两种函数声明，根据 ES5 的规定都是非法的。

但是，浏览器没有遵守这个规定，为了兼容以前的旧代码，还是支持在块级作用域之中声明函数，因此上面两种情况实际都能运行，不会报错。

ES6 引入了块级作用域，明确允许在块级作用域之中声明函数。ES6 规定，块级作用域之中，函数声明语句的行为类似于`let`，在块级作用域之外不可引用。

```javascript
function f() { console.log('I am outside!'); }

(function () {
  if (false) {
    // 重复声明一次函数f
    function f() { console.log('I am inside!'); }
  }

  f();
}());
```

上面代码在 ES5 中运行，会得到“I am inside!”，因为在`if`内声明的函数`f`会被提升到函数头部，实际运行的代码如下。

```javascript
// ES5 环境
function f() { console.log('I am outside!'); }

(function () {
  function f() { console.log('I am inside!'); } //提升了
  if (false) {
  }
  f();
}());
```

ES6 就完全不一样了，理论上会得到“I am outside!”。因为块级作用域内声明的函数类似于`let`，对作用域之外没有影响。但是，如果你真的在 ES6 浏览器中运行一下上面的代码，会报错

```javascript
// 浏览器的 ES6 环境
function f() { console.log('I am outside!'); }

(function () {
  if (false) {
    // 重复声明一次函数f
    function f() { console.log('I am inside!'); } //ES6 不会提升
  }

  f();
}());
// Uncaught TypeError: f is not a function
```

如果改变了块级作用域内声明的函数的处理规则，显然会对老代码产生很大影响。为了减轻因此产生的不兼容问题，ES6 在[附录 B](http://www.ecma-international.org/ecma-262/6.0/index.html#sec-block-level-function-declarations-web-legacy-compatibility-semantics)里面规定，浏览器的实现可以不遵守上面的规定，有自己的[行为方式](http://stackoverflow.com/questions/31419897/what-are-the-precise-semantics-of-block-level-functions-in-es6)。

- 允许在块级作用域内声明函数。
- 函数声明类似于`var`，即会提升到全局作用域或函数作用域的头部。
- 同时，函数声明还会提升到所在的块级作用域的头部。

注意，上面三条规则只对 ES6 的浏览器实现有效，其他环境的实现不用遵守，还是将块级作用域的函数声明当作`let`处理。

根据这三条规则，浏览器的 ES6 环境中，块级作用域内声明的函数，行为类似于`var`声明的变量。上面的例子实际运行的代码如下。

```javascript
// 浏览器的 ES6 环境
function f() { console.log('I am outside!'); }
(function () {
  var f = undefined;
  if (false) {
    function f() { console.log('I am inside!'); }
  }

  f();
}());
// Uncaught TypeError: f is not a function
```

考虑到环境导致的行为差异太大，应该避免在块级作用域内声明函数。如果确实需要，也应该写成函数表达式，而不是函数声明语句。

```javascript
// 块级作用域内部的函数声明语句，建议不要使用
{
  let a = 'secret';
  function f() {
    return a;
  }
}

// 块级作用域内部，优先使用函数表达式
{
  let a = 'secret';
  let f = function () {
    return a;
  };
}
```

**插一句==============================================================>>>>>>>>>>>>>>**

#### 定义函数的方法

* 函数声明
* 函数表达式
* new Function构造函数

**函数声明的格式**

```javascript
function functionName(arg1,arg2,arg3.....){
    <!-- function body -->
}
```

函数表达式

* 函数表达式典型格式

```javascript
var  variable=function(arg1, arg2, ...){
            <!-- function body -->
}
```

* 包含名称（括弧，函数名）的函数表达式

```javascript
var  variable=function functionName(arg1, arg2, ...){
        <!-- function body -->
}
```

*像上面的带有名称的函数表达式可以用来递归*

```
var  variable=function functionName(x){
        if(x<=1)
            return 1;
        else
            return x*functionName(x);
}
```

**声明提升概念**

**变量在声明它们的脚本或函数中都是有定义的，变量声明语句会被提前到脚本或函数的顶部。但是，变量初始化的操作还是在原来var语句的位置执行，在声明语句之前变量的值是undefined。**

1. 变量声明会提前到函数的顶部；
2. 只是声明被提前，初始化不提前，初始化还在原来初始化的位置进行初始化；
3. 在声明之前变量的值是undefined。

例子

```javascript
var handsome='handsome';
function handsomeToUgly(){
    alert(handsome);
    var handsome='ugly';
    alert(handsome);
}
handsomeToUgly();
```

**先输出`undefined`，然后输出`ugly`。**

##### 函数声明提前

```javascript
sayTruth();<!-- 函数声明 -->
function sayTruth(){
    console.log('myvin is handsome.'); //myvin is handsome.
}

sayTruth();<!-- 函数表达式 -->
var sayTruth=function(){
    console.log('myvin is handsome.');  //TypeError: sayTruth is not a function
}
```

**函数声明提前的时候，函数声明和函数体均提前了。**

**函数声明是在预执行期执行的，就是说函数声明是在浏览器准备执行代码的时候执行的。因为函数声明在预执行期被执行，所以到了执行期，函数声明就不再执行**



##### 函数表达式为什么不能声明提升

函数表达式就是把函数定义的方式写成表达式的方式，就是把**一个函数对象赋值给一个变量**，所以我们把函数表达式写成这个样子：

```javascript
var  variable=function(arg1, arg2, ...){
                    <!-- function body -->
}
  //====================================================                  
var varible=5
```

一个是把一个值赋值给一个变量，一个是把函数对象赋值给一个变量，所以对于函数表达式，变量赋值是不会提前的，即`function(arg1, arg2, ...){<!-- function body -->}`是不会提前的，所以函数定义并没有被执行，所以函数表达式不能像函数声明那样进行函数声明提前。

```javascript
sayTruth();
function sayTruth(){console.log('myvin is handsome')};
function sayTruth(){console.log('myvin is ugly')};

//myvin is ugly
```

因为函数声明提前，所以函数声明会在代码执行前进行解析，执行顺序是这样的，先解析`function sayTruth(){alert('myvin is handsome')}`，在解析`function sayTruth(){alert('myvin is ugly')}`，覆盖了前面的函数声明，当我们调用`sayTruth()`函数的时候，也就是到了代码执行期间，声明会被忽略，所以自然会输出`myvin is ugly`

**继续=====================================================================>>>>>>**

另外，还有一个需要注意的地方。ES6 的块级作用域必须有大括号，如果没有大括号，JavaScript 引擎就认为不存在块级作用域。

```javascript
// 第一种写法，报错
if (true) let x = 1;   //SyntaxError: Lexical declaration cannot appear in a single-statement context  词法声明不能出现在单个语句上下文中

// 第二种写法，不报错
if (true) {
  let x = 1;
}
```

上面代码中，第一种写法没有大括号，所以不存在块级作用域，而`let`只能出现在当前作用域的顶层，所以报错。第二种写法有大括号，所以块级作用域成立。

函数声明也是如此，严格模式下，函数只能声明在当前作用域的顶层。

```javascript
// 不报错
'use strict';
if (true) {
  function f() {}
}

// 报错
'use strict';
if (true)
  function f() {}
//SyntaxError: In strict mode code, functions can only be declared at top level or inside a block.  在严格模式代码中，只能在顶层或块内部声明函数。
```

### const命令

#### 基本用法

`const` 声明一个只读的常量，一旦声明，常量的值就不能改变

```javascript
const PI = 3.1415
PI

PI = 3
//TypeError: Assignment to constant variable.  对常量变量的赋值。
```

`const`声明的变量不得改变值，这意味着，`const`一旦声明变量，就必须立即初始化，不能留到以后赋值

```javascript
const p;
//SyntaxError: Missing initializer in const declaration  const声明中缺少初始值设定项
```

对于`const`来说，只声明不赋值，就会报错。

`const`的作用域与`let`命令相同：只在声明所在的块级作用域内有效。

```javascript
if(true){
     const p = 1
   }
console.log(p) //  ReferenceError: p is not defined
```

上面代码在常量`MAX`声明之前就调用，结果报错。

`const`声明的常量，也与`let`一样不可重复声明。

```javascript
var message = 'hello!'
let age = 25

//==================

const message = 'hahaha!'
const age = 30
//SyntaxError: Identifier 'message' has already been declared  标识符“message”已声明  Identifier 'age' has already been declared     
```

#### 本质

`const`实际上保证的，并不是变量的值不得改动，而是变量指向的那个内存地址所保存的数据不得改动。对于简单类型的数据（数值、字符串、布尔值），值就保存在变量指向的那个内存地址，因此等同于常量。但对于复合类型的数据（主要是对象和数组），变量指向的内存地址，保存的只是一个指向实际数据的指针，`const`只能保证这个指针是固定的（即总是指向另一个固定的地址），至于它指向的数据结构是不是可变的，就完全不能控制了。因此，将一个对象声明为常量必须非常小心。

```javascript
const foo = {}
foo.age = 12
foo.age = 11
console.log(foo.age) //11
//报错
foo = {}  // TypeError: Assignment to constant variable.
```

上面代码中，常量`foo`储存的是一个地址，这个地址指向一个对象。不可变的只是这个地址，即不能把`foo`指向另一个地址，但对象本身是可变的，所以依然可以为其添加新属性。

下面是另一个例子。

```javascript
const a = []
a.push('aaaa') //不报错
a.length = 0 //不报错
a = ['aaaaaaaaa'] // 报错
```

上面代码中，常量`a`是一个数组，这个数组本身是可写的，但是如果将另一个数组赋值给`a`，就会报错。

如果真的想将对象冻结，应该使用`Object.freeze`方法。

```javascript
const foo = Object.freeze({})

foo.prop = 123
```

上面代码中，常量`foo`指向一个冻结的对象，所以添加新属性不起作用，严格模式时还会报错。

除了将对象本身冻结，对象的属性也应该冻结。下面是一个将对象彻底冻结的函数。

```javascript
var constantize = (obj) => {
  Object.freeze(obj);
  Object.keys(obj).forEach( (key, i) => {
    if ( typeof obj[key] === 'object' ) {
      constantize( obj[key] );
    }
  });
};
```

#### ES6声明变量的6种方法

ES5 只有两种声明变量的方法：`var`命令和`function`命令。ES6 除了添加`let`和`const`命令，另外两种声明变量的方法：`import`命令和`class`命令。所以，ES6 一共有 6 种声明变量的方法。

#### 顶层对象的属性

顶层对象，在浏览器环境指的是`window`对象，在 Node 指的是`global`对象。ES5 之中，顶层对象的属性与全局变量是等价的。

```javascript
window.a = 1;
a // 1

a = 2;
window.a // 2
```

上面代码中，顶层对象的属性赋值与全局变量的赋值，是同一件事。

ES6 为了改变这一点，一方面规定，为了保持兼容性，`var`命令和`function`命令声明的全局变量，依旧是顶层对象的属性；另一方面规定，`let`命令、`const`命令、`class`命令声明的全局变量，不属于顶层对象的属性。也就是说，从 ES6 开始，全局变量将逐步与顶层对象的属性脱钩。

```javascript
var a = 1;
// 如果在 Node 的 REPL 环境，可以写成 global.a
// 或者采用通用方法，写成 this.a
window.a // 1

let b = 1;
window.b // undefined
```

上面代码中，全局变量`a`由`var`命令声明，所以它是顶层对象的属性；全局变量`b`由`let`命令声明，所以它不是顶层对象的属性，返回`undefined`。