## Vue中eCharts简单使用

### 1.下载引入

	#### 	npm引入

	>npm install echarts --save

	#### 	main.js调用

````javascript
import echarts from 'echarts' //引入echarts
import 'element-ui/lib/theme-chalk/index.css'  //引入css样式

Vue.prototype.$echarts = echarts //挂载到原型上
````

### 2.简单使用

​	在vue文件中

````javascript
<template>
	<div id="main" :style="{width:'600px',height:'400px'}" ref="myCharts"></div>   
</template>
<script>
export default {
    data(){
        return {
            
        }
    },
    //echarts放在mounted中去使用 用$refs是非响应式的
    mounted(){
        const myCharts = this.$echarts.init(this.$refs.myCharts)
        //或者用 const myCharts = this.$echarts.init(document.getElementById('main'))
        let options = {
            title: {
                text: '未来一周气温变化',//图顶部的标题
                subtext: '纯属虚构'//副标题
            },
            tooltip: {  //鼠标悬浮提示文字
                trigger: 'axis'
            },
            legend: {
                data: ['最高气温','最低气温']
            },
            xAxis: [{ //X轴数据
                type: 'category',
                boundartGap: false,
                data: ['周一','周二','周三','周四','周五','周六','周日']
            }],
            yAxis: [{ //Y轴数据
                type: 'value',
                axisLabel: {
                    formatter: '{value} ℃'
                }
            }],
            series:[ //驱动图表生成的数据内容数组。几条折线，数组中就有几个对应对象，来表示对应的折线
                {
                    name:'最高气温',
                    type:'line',
                    data: [11, 11, 15, 13, 12, 14, 18],
                },
                {
                    name:'最低气温',
                    type: 'line',
                    data: [1, -2, 2, 5, 3, 2, 0]
                }
            ]
        }
        myCharts.setOption(options)
    }
}
</script>
````

### 3.echarts一些关键字

具体看官网的[文档配置项](https://echarts.apache.org/zh/option.html#title)

````javascript
setOption({
    title:{  //标题组件
        text:'',  //（string）主标题  支持 \n 换行
        subtext:'',  //（string）副标题
        id:'',  //（string）组件ID，默认不指定，指定则可用于在option或者API中引用组件
        show: true,  //(boolean) 是否显示标题组件
        link: '', //(string) 主标题文本超链接
        target:'',  //(string) 指定窗口打开主标题超链接
        textStyle:{ //（object）标题样式
            color: '#332', //(color)主标题文字的颜色
            fontStyle：'normal', //(string) 主标题文字字体的风格  可选 'normal','italic','oblique'
            fontWeight:normal // (string number)主标题文字字体的粗细 可选'normal','bold','bolder','lighter',100..
            fontFamily:'sans-serif', //(string) 主标题文字的字体系列
            fontSize：18, //(number) 主标题文字大小
           	.......
        }
    },
    legend:{ // 图例组件
        type:'plain'(普通图例)/'scroll'(可滚动翻页的图例)
        .....
    }
})
````

### 4.vue中使用echarts的注意事项

​	1）在你需要用到echart的地方先设置一个div的id、宽高，

	> ```
	> <div ref="chart" style="width:100%;height:376px"></div>
	> ```

​	2）然后我们要在mounted生命周期函数中实例化echarts对象。因为我们要确保dom元素已经挂载到页面中。

​	3) 直观关键字

![](https://s1.ax1x.com/2020/08/04/a0OJJA.png)

