## vue的插槽slot

### 一、匿名插槽

在vue脚手架组件引用不同，具体自己区分，默认在html中引入了vue，不是用vue脚手架，插槽默认跟template模板标签配合使用

**举例：**

自定义一个组件 名为 tag1

```javascript
//组件模板为
var tag1 = {
    template:`<dl> 
                <dt>狮子</dt>
                <dd>是一种生存在非洲与亚洲的大型猫科动物</dd>
              </dl>`
}
.......定义一个局部组件
components:{
    tag1
}
.......
```

然后在结构中引入组件<tag1></tag1>

![img](https://upload-images.jianshu.io/upload_images/13748290-bf9a97b65f6b9fd8.jpg?imageMogr2/auto-orient/strip|imageView2/2/w/652/format/webp)

此时此刻，如果我们想进一步丰富template模板中展示内容的灵活性，可以借助插槽slot
*需求：如果我们想把模板中的狮子替换成老虎*

**实现**

```javascript
<tag1>老虎</tag1>  //将要写的内容写在标签内，像HTML标签一样

var tag1mes = {
  template:`<dl> 
              <dt><slot></slot></dt>  //添加的位置上，增加<slot></slot>标签
              <dd>是一种生存在非洲与亚洲的大型猫科动物</dd>
            </dl>`
}
```

再看渲染

![img](https://upload-images.jianshu.io/upload_images/13748290-e382d165b025db84.jpg?imageMogr2/auto-orient/strip|imageView2/2/w/652/format/webp)



此外可以添加默认值

```javascript
var tag1mes = {
  template:`<dl> 
    <dt><slot>狮子</slot></dt>  //slot中为默认值
    //slot中的是默认的，<tag1>标签内为用户设置的，【设置没有显示默认，设置有，显示设置的】
    <dd>是一种生存在非洲与亚洲的大型猫科动物</dd>
  </dl>`
}
```

呈现效果

<tag1>老虎</tag1>，渲染标题为老虎，
<tag1></tag1> 渲染结果为，默认的狮子，
最后，由于这种<slot></slot>插槽没有name属性，因此叫【匿名插槽】

### 二、具名插槽

*需求：如果不仅替换标题，还要替换标题的内容描述，怎么办？*
 显然
 <tag1> 老虎， 大型猫科动物；毛色浅黄或棕黄色，满身黑色横纹；头圆、耳短，耳背面黑色，中央有一白斑甚显著。 </tag1>
 怎么分开对应dt,dd标签是个问题

v-slot 语法格式

> ```javascript
> v-slot:slotName  //slotName不能加双引号“”
> ```

匿名插槽 v-slot默认指向default

> v-slot:default



修改代码如下

```javascript
<tag1>
        <template v-slot:tit>老虎</template>
        <template v-slot:mes>大型猫科动物；毛色浅黄或棕黄色，满身黑色横纹；头圆、耳短，耳背面黑								色，中央有一白斑甚显著。
        </template>
</tag1>

template:` <dl>
      <dt><slot name="tit">狮子</slot></dt>
      <dd><slot name="mes">是一种生存在非洲与亚洲的大型猫科动物</slot></dd>
</dl>`
```

**简写方式**

```javascript
v-slot:header 简写成 #header
v-slot:default 简写成 #default
```

**注意：**

1. **父级的填充内容如果指定到子组件的没有对应名字插槽，那么该内容不会被填充到默认插槽中。**

2. **如果子组件没有默认插槽，而父级的填充内容指定到默认插槽中，那么该内容就不会填充到子组件的任何一个插槽中。**

3. **如果子组件有多个默认插槽，而父组件所有指定到默认插槽的填充内容，将会全都填充到子组件的每个默认插槽中。**

### 三、作用域插槽

语法格式

```javascript
v-slot:tit="slotProps"  //slotProps为自定义的变量名，指向子组件中的data函数返回值
//将具名插槽赋值，很简洁；
//不仅完成插槽指向，还完成了数据挂载
```

例子改写

```javascript
//父组件
<tag1>
     <template v-slot:tou="propsTxt">
         {{propsTxt.txt1}}
     </template>
<!-- <template v-slot:jiao>aaaaa</template> -->
</tag1>
//子组件
<dl>
  <dt><slot name="tou" :txt1="txt1">狮子</slot></dt>
  <dd><slot name="jiao">是一种生存在非洲与亚洲的大型猫科动物</slot></dd>
</dl>

....data中....

	data(){
        return {
            txt1:'我是子组件里的数据: 猫头鹰'
        }
  	}
```

![img](https://upload-images.jianshu.io/upload_images/13748290-90725c0620868376.jpg?imageMogr2/auto-orient/strip|imageView2/2/w/652/format/webp)



**作用域插槽图解**

![img](https://upload-images.jianshu.io/upload_images/13748290-aa200acf96cbd470.jpg?imageMogr2/auto-orient/strip|imageView2/2/w/748/format/webp)

插槽父子之间的传值

父组件

```javascript
<template #swiperSlide='{dataMes}'>
	{{datames}}
</template>
```

子组件

```xml
<slot name="swiperSlide" :dataMes='dataMes'></slot>
```

说明：
父组件通过 #slotName 获取子组件 ，
通过 { 数据名 } 获取子组件中的数据