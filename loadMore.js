import {Route, HashRouter as Router, withRouter, Link} from "react-router-dom";
import React, {Component} from "react";
import {connect} from "react-redux";

class LoadMore extends Component {

  state = {
    dataList:['hahha','ggggg','hhhhhh'],
    disTance:25
  }
  loadRef = React.createRef()

  componentDidMount(){
    
    // window.addEventListener('scroll',this.handleScroll)
    
    let dom = document.getElementById('a')

    dom.addEventListener('scroll',this.onScroll)
  }
  loadMore = () => {
    let arr = this.state.dataList;
    let item = []
    for(let i = 0; i < 3; i++){
      item.push(`${i}`)
    }
    arr.push(...item)
    this.setState({
      dataList:[...arr]
    },() =>{
      console.log('更新后',this.state.dataList)
    })
  }

  onScroll = (e) =>{
    let scrollTop = e.target.scrollTop //向上滚动距离
    this.offsetHeight = Math.ceil (e.target.getBoundingClientRect().height) //固定可视滚动高度
    this.scrollHeight = Math.ceil(e.target.scrollHeight) //总的内容高度
    let currentHeight = scrollTop + this.offsetHeight + this.state.disTance
    console.log('eeee',scrollTop,this.offsetHeight,currentHeight,this.scrollHeight)
    
    if(currentHeight < this.scrollHeight && this.isReachBottom){ // 747 617 100 
      this.isReachBottom = false
    }
    if(this.isReachBottom){
      return
    }
    if (currentHeight > this.scrollHeight) {
      this.isReachBottom = true
      console.log('触底')
      this.loadMore()
    }
    // let scrollTop = e.target.scrollTop //0 + + + ++ +
    // let scrollHeight = e.target.scrollHeight //618 固定
    // let offsetHeight = Math.ceil(e.target.getBoundingClientRect().height) // 618
    // let currentHeight = scrollTop + offsetHeight
    // console.log('scrollTop',scrollTop,'offsetHeight',offsetHeight,'currentHeight',currentHeight,'scrollHeight',scrollHeight)
    // if (currentHeight > scrollHeight) {
    //     console.log('触底')
    // }
  }
    

  render() {
    return (
      <div style={{background:'#fff',overflow:'hidden'}} id='contBox'>
        <div style={{background:'blue',height:'1rem'}}>title</div>
        <div style={{overflowY:'scroll',height:'calc(100vh - 1rem)'}} id='a'>
          {
            this.state.dataList.map((v,i) => {
              return <div style={{height:'2rem',textAlign:'center',lineHeight:'2rem',background:'skyblue'}} key={i}>{v}</div>
            })
          }
          <div style={{textAlign:'center',margin:'0.3rem 0'}} onClick={this.loadMore} ref={this.loadRef} id='aaa'>加载更多</div>
        </div>
      </div>
    )
  }
}


function mapStateToProps(state) {
  const {searchData, router} = state;
  return {
      searchData, router
  }
}

export default connect(mapStateToProps)(withRouter(LoadMore))