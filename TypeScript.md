## TypeScript

### 起步 

安装

> npm i -g typescript

编译

> tsc xxx.ts

约定

> 以 .ts为后缀，编写react时，以.tsx为后缀

主流IDE中都支持TS，包括代码补全，接口提示，跳转定义，重构

几种类型声明

````javascript
var str:string = 'hello';

var num:number = 1;

var flag:boolean = true;

var un:undefined = undefined;

var nul:null = null;

str = undefined;

//void 表示无返回值
var callback = function ():void{

}


//any 表示任意值类型
var num:any = 1;

num = true;

num = '3';

var num2;//没有赋值操作，就会认为是任意值类型，等价于 var num2:any;

num2 = 1;

num2 = '1';

````

接口

````javascript
interface Istate {
    name:string,
    age:number
}
var obj1:Istate;
obj1 = {name:"张三",age:10}

//可选属性
interface tate2 {
    name:string,
    age?:number
}
var obj2:tate2;
obj2 = {name:"哈哈",age:11}
obj2 = {name:"李四"}

// 属性个数不确定时
interface tate3{
    name:string,
    age?:number, //?表示 可以不填写 可有可无
    [propName:string]:any
}

var obj3:tate3;
obj3 = {name:"aa",age:10,sex:"bb",haha:"10"}

// 只读属性
interface tate4{
    name:string,
    readonly age:number
}
var obj4:tate4 = {name:"afgaga",age:20}
obj4.name = "李四"
// obj4.age = 18

````

联合类型

````javascript
//联合类型
var muchtype:string|number = "hello";
muchtype = 10;
console.log(muchtype.toString())
````

数组类型

``````javascript
//数组表示
// 类型 + []
var arr:number [] = [1,2,3]
var arr2:string [] = ["1","2","3"]
var arr3:any [] = [1,"2",true]

// 数组泛型 Array <elemType>
var arrType:Array<number> = [1,2,3];
var arrType2:Array<string> = ["1","2","3"];
var arrType3:Array<any> = [1,"2",true]

// 接口表示法

interface Istate {
    username:string,
    age:number
}

interface IArr {
    [index:number]:Istate
}
// var arrType4:IArr = [1,2,3]
var arrType5:IArr = [{username:"张三",age:10}]
var arrType6:Array<Istate> = [{username:"张三",age:10}]
var arrType7:Istate[] = [{username:"张三",age:10}]
``````

函数类型

````javascript
// 声明式函数
// 约束
function funType(name:string,age:number):number{
    return age
}
var ageNum:number = funType("张三",18)

// 函数参数不确定

function funType2(name:string,age:number,sex?:string):number{
    return age
}
var ageNum2:number = funType2("张三",18,"男")


// 函数参数的默认值

function funType3(name:string="张三",age:number=18):number{
    return age
}

// 表达式函数
var funType4 = function(name:string,age:number):number{
    return age
}

// 函数的约束   规定左边是个函数，接受两个参数，返回number  右边就是这个函数的定义
var funType5:(name:string,age:number)=>number = function(name:string,age:number):number{
    return age
}

// 接口方式约束
interface funType6{
    (name:string,age:number):number
}
var funType6:funType6 = function(name:string,age:number):number{
    return age
}

// 对于联合类型的函数。可以采用重载的方式
// 输入是number 输出也是number
// 输入是string 输出也是string

function getValue(value:number):number;
function getValue(value:string):string;
function getValue(value:string|number):string|number{
    return value
}

let a:number = getValue(1)
let b:string = getValue("1")
````

类型断言

````javascript
// let num:string|number = "10";
// num = 20;
// console.log(num.length)

// 类型断言  只能断言联合类型中存在的类型
function getAssert(name:string|number){
    
    //在jsx语法不可取
    // return (<string>name).length 
    
    return (name as string).length
    // return (name as boolean).length  //错误的
}
````

类型别名

````javascript
// 类型别名
// var str:string|number = "10";

type strType = string|number|boolean;
var str:strType = "10"
str = 10
str = true


// 可以对于接口也采用类型别名
interface muchType1{
    name:string
}
interface muchType2{
    age:number
}
type muchType = muchType1 | muchType2;

var obj:muchType = {name:"张三"}
var obj2:muchType = {age:10}
var obj3:muchType = {name:"李四",age:10}


// 限制字符串的选择
type sex = "男" | "女"
function getSex(s:sex):string{
    return s
}
getSex("男")
````

枚举

````javascript
// 使用枚举可以定义一些有名字的数字常量
//默认从0 开始
enum Days{
    Sun=3,
    Mon,
    Tue,
    Wed,
    Thu,
    Fri,
    Sat
}
console.log(Days.Sun)//0
console.log(Days.Sat)//9
console.log(Days) //枚举类型会被编译成一个双向映射的对象
console.log(Days[0] === "Sun") 
````

类修饰符

````javascript
// 创建person类  
/**
 * public修饰的属性或者方法是共有的，可以再任何地方被访问到，默认所有的属性或者方法都是public
 * private 修饰的属性或者方法是私有的，不能再声明在他的类外面访问
 * protected 修饰的属性或者方法是受保护的，它和private类似
 */
class Person{
    private name="张三"
    protected age=18
    say(){
        console.log("我的名字是"+this.name,"我的年龄为"+this.age)
    }
}

// 创建person实例

var p =new Person()
p.say()
// //prevate属性只能在类的内部进行访问
// console.log(p.name) //当一个类成员变量没有修饰的时候，外界是可以进行访问的，默认就是public进行修饰


// 创建Child子类
// 一旦父类将属性定义成私有的以后，子类就不可以进行访问了
// 父类的属性定义成受保护的之后
class Child extends Person{
    callParent(){
        console.log(super.age)  //这里可访问到
        super.say()
    }
    static test(){
        console.log("test")
    }
}

var c = new Child()
// c.callParent()
// // console.log(c.age) //报错 属性“age”受保护，只能在类“Person”及其子类中访问。   子类继承了父类，但是没有办法直接获取到父类私有的属性或者受保护的属性
// console.log(c.say())  //子类继承了父类，子类就可以访问到父类的公开的属性或者方法了

console.log(Child.test()) //类的静态方法，不允许使用this
````

泛型

````javascript
/**
 * 泛型是指在定义函数、接口或类的时候，不预先指定具体类型，而在使用的时候在指定类型的一种特性
 */

 //没有确切的定义返回值类型，运行的数组每一项都是可以是任意类型
//  function createAray(length:number,value:any):Array<any>{
//      let arr = [];
//      for(var i = 0; i <length; i++){
//          arr[i] = value
//      }
//      return arr;
//  }
//  createAray(3,1);


//泛型改造
//不传的时候根据类型进行倒推
//泛型可以帮助我们限定约束规范
function createAray<T>(length:number,value:T):Array<T>{
         let arr = [];
         for(var i = 0; i <length; i++){
             arr[i] = value
         }
         return arr;
}

// var strArray:string[] = createAray<string>(3,'1') 

// var strArray:string[] = createAray<string>(3,1) //报错

// var numArray:number[] = createAray(3,1)


//接口泛型
interface ICreate{
    <T>(name:string,value:T):Array<T>
}

let func:ICreate;
func = function <T>(name:string,value:T):Array<T> {
    return []
}

var strArr:string[] = func('gagaga','2')

var ss:number[] = func('gagaga',1)
````

