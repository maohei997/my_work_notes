## 父子通信

**props down**

```javascript
//父组件
<template>
  <div>
      <Users :users="users"></Users>
  </div>
</template>

<script>
import Users from '../components/Users'
export default {
    data(){
        return{
            users:['HaHa','XiXi','WaWa']
        }
    },
    components:{
        Users,
    }
}
</script>

<style>

</style>




//子组件
<template>
  <div class="hello">
      <ul>
          <li v-for="(user,index) in users" :key="index">{{user}}</li>
      </ul>
  </div>
</template>

<script>
export default {
    props:{
        users:{
            type:Array,
            required:true
        }
    }
}
</script>

<style>

</style>

//效果
HaHa
XiXi
WaWa
```

## 子父通信

**events up**   事实上还是父组件自己定义了方法 改变了自己的值 

子组件先是定义一个事件,然后经过this.$emit()事件  弹射出一个方法  `（感觉跟函数的return返回值类似？？）`

> $emit()  有两个参数   第一个参数是弹射出去的，父组件接收到的方法
>
> ​									第二个参数是传给父组件的值，可以是一个值，也可以是多个值（多个值用数									组形式）

```javascript
//子组件
<template>
  <div class="hello">
      <h1 @click="changeTitle">{{title}}</h1>
  </div>
</template>

<script>
export default {
    data(){
        return{
            title:'Vue.js Demo'
        }
    },
    methods:{
        changeTitle(){
            this.$emit('titleChanged','子向父组件传值')
        }
    }
}
</script>

<style>

</style>



//父组件
<template>
  <div>
      <Users @titleChanged="updateTitle"></Users>
      <h2>{{title}}</h2>
  </div>
</template>

<script>
import Users from '../components/Users'
export default {
    data(){
        return{
           title:'传递的是一个值'
        }
    },
    components:{
        Users,
    },
    methods:{
        updateTitle(e){
            console.log(e)  //是数组形式的话 这里可以选择用  e[index] 的形式取值
            this.title = e
             //console.log(e)
            //for(let i = 0; i <e.length; i++){
              //  this.title = e[i]
         //   }
        }
    }
}
</script>

<style>

</style>
```

## $emit/$on

**这种方法通过一个空的Vue实例作为中央事件总线（事件中心），用它来触发事件和监听事件,巧妙而轻量地实现了任何组件间的通信，包括父子、兄弟、跨级**。**当我们的项目比较大时，可以选择更好的状态管理解决方案vuex。**

### 实现方式

```javascript
	var Event=new Vue();
    Event.$emit(事件名,数据);
    Event.$on(事件名,data => {});
```

### 实现例子

假设兄弟组件有三个，分别是A、B、C组件，C组件如何获取A或者B组件的数据