## 关于Vuex的一些整理

vuex就是vue提供的一个状态管理工具，就是vue提供存储一些必要数据的仓库。

vue脚手架默认安装 vuex

main.js中  默认全局使用了vuex的仓库

```javascript
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
```

vuex包含了四个基础部分（四大金刚）：state,mutations,actions,getters；另外还有一个modules,引入外部自定义的vuex文件。

**Vuex包括了四个辅助函数： mapState，mapMutations，mapActions，mapGetters。就是对于store中的state，mutations，actions，getters的映射关系**

**可以简单总结一下辅助函数通过vuex使用，比喻成映射关系为： mapState/mapGettes--->computed ； mapAcions/mapMutations---->methods  **



### 一、 vuex的state

相当于普通vue文件的  data  存放着需要用到的数据。

存储在 Vuex 中的数据和 Vue 实例中的 `data` 遵循相同的规则，例如状态对象必须是纯粹的。

在store文件夹中的 `index.js` 中

```javascript
 state: {
    count:0,
    list:[1,5,8,10,30,50]
  },
```

在需要用到的 `.vue`文件中

为了简洁书写，放在计算属性中调用

```javascript
//html部分
{{count}}

//js部分
computed:{
    count(){
        return this.$store.state.count
    }
}
```

也可以引入vuex的辅助函数 mapState     效果是一样的

在 `.vue` 的script中

```javascript
//html部分
{{count}}

//js部分
import {mapState} from 'vuex'

然后computed

computed: mapState({
    count: state => state.count
})
```

如果要跟本页面的其他计算属性 同时用，可以用对象展开运算符  ...mapState

```javascript
//html部分
{{count}}

//js部分
import {mapState} from 'vuex'

然后computed

computed:{
    //其他计算属性
    xxx(){
        
    },
    zzz(){
        
    },
    ...mapState({
        count: state => state.count
    })
}
```

**注意：**

​		严格模式下......

​		在组件内，来自store的数据只能读取，不能手动改变。改变store中数据的唯一方式就是显示的提交mutations。

### 二、 vuex的mutations

Vue官网原话：

**更改 Vuex 的 store 中的状态的唯一方法是提交 mutation。Vuex 中的 mutation 非常类似于事件：每个 mutation 都有一个字符串的 事件类型 (type)和 一个 回调函数 (handler)。这个回调函数就是我们实际进行状态更改的地方，并且它会接受 state 作为第一个参数**

**只是做简单的数据修改（例如n++），它没有涉及到数据的处理，没有用到业务逻辑或者异步函数，可以直接调用mutations里的方法修改数据。**



mutations是用来修改state中的数据的。可以在此写方法，然后修改state中的值。在 `.vue` 文件里，定义在methods中。然后通过this.$store.commit ( ' mutations方法名 ' )  如果mutations里的方法有第二个参数，则commit（‘方法名’ ， 参数）

例如在store文件夹下的 `index.js` 定义mutations

```javascript
state: {
    count:0,
    list:[1,5,8,10,30,50]
  },

mutations: {
    plusCount(state,n=1){
      state.count += n
    },
    reduceCount(state){
      state.count --
    }
  },
```

在 `.vue`  文件夹下

```javascript
//html
{{$store.state.count}} // 0 1 2 3 4 5 6 7 8 9.... || 2 4 6 8 10 ....
<button @click="btnTap">点击修改vuex的count</button> // 每次+1

//js
methods:{
     btnTap(){
            this.$store.commit('plusCount')
         	
         	or
            
            this.$store.commit('plusCount',2)
        },
}
```

也可以用辅助函数  mapMutations  **把mutations里面的方法映射到methods中**

```javascript
//html
{{$store.state.count}} // 0 1 2 3 4 5 6 7 8 9.... || 2 4 6 8 10 ....
<button @click="btnTap">点击修改vuex的count</button> // 每次+1

//js
import {mapState,mapMutations} from 'vuex'

methods:{
     btnTap(){
           this.plusCount() //可以传参
         	or
           this.plusCount(2) //可以传参
        },
     ...mapMutations({
         plusCount:'plusCount' // mutations里面的方法名称
     })
}
```

在store的`index.js`  中mutations还能这样定义

```javascript
store 中的index.js

.....
mutations:{
	plusCount(state,params){
        state.count += params.count
    }
}

//vue文件中
{{$store.state.count}} // 10 20 30 40 50 60......
<button @click="btnTap">点击修改vuex的count</button> // 每次+1

//js
btnTap(){
	this.$store.commit({
        type:'plusCount',
        count:10
    })
}
```

### 三、 vuex的actions

​	相对比vuex的mutations，actions功能更强一些。在mutations中不能存在异步操作，但是actions可以。actions是用来提交mutations的。

actions里传入两个参数 context  和 playload , actions在组件中是通过$store.dispatch触发,用法this.$store.dispatch ( ' actionsName ' ,  playload) 

**在 store中的index.js**

```javascript
.....
state: {
    count:0,
},
mutations:{
     plusCount(state,n = 1){
      state.count += n
    },
},
actions:{
    changeCount(context,playload){
        return new Promise ((resolve,reject) => {
            setTimeout(() => {
                context.commit('plusCount',playload)
                resolve()
            },1000)
        })
    }
}
```

**在vue文件中**

```javascript
//html
{{$store.state.count}}  // 5 10 15 20 25 30......
<button @click="btnTap(5)">点击修改vuex的count</button>

//JS
methods:{
    btnTap(num){
        this.$store.dispatch('changeCount',num)
    }
}
```

**也可以用mapActions辅助函数**

```javascript

//html
{{$store.state.count}}
<button @click="changeCount(5)">点击修改vuex的count</button> 	//方法一
<button @click="btnTap(5)">点击修改vuex的count</button> 	//方法二



//JS
//引入辅助函数
import {mapActions} from 'vuex'

//使用方法 1 ：
methods:mapActions(['changeCount']) //名字为actionsName  这样methods不能定义其他函数

//使用方法 2 ：
methods:{
    ...mapActions({
        btnTap:'changeCount' //名字为actionsName  可跟其他本页面函数共同存在
    })
}
```

### 四、 vuex的getters

```
getters类似于组件里面computed同时也是监听属性的变化，当state中的属性发生改变的时候就会触发getters里面的方法。

getters里面的每一个方法中都会有一个参数 state。

getters 中的方法，可以依赖其中的其他方法，把其他方法作为第二个参数。
```



**注意**

**！！只有深拷贝才会触发getters相对应的方法！！**



**举例，vuex中**

```javascript
......
state:{
	numList:[1,2,3,4,5,6,7]
},
getters:{
    filterList: state => {
        return state.numList.filter(v => v>3)
    }，
    
    filterListLength: (state,getters) => {
      return getters.filterList.length
    }
}
    
```

vue文件中

```javascript
//html
{{list}} //[4,5,6,7]
{{listLength}} // 4
//JS
computed:{
    
    list(){
        return this.$store.getters.filterList
    },
        
    listLength(){
        return this.$store.getters.filterListLength
    }
}
```

mapGetters辅助函数

```javascript
//html
{{list}} //[4,5,6,7]
{{listLength}} // 4

//JS
import {mapGetters} from 'vuex'

computed:{
    ...mapGetters({
        list:'filterList',
        listLength:'filterListLength'
    })
}
```

### 五、 vuex的modules

为了避免state中代码比较臃肿，可以将store分割到不同模块。每个module都是一个小型的store，可以统一集中处理。

1、每个模块都相当于一个小型的Vuex

2、每个模块里面都会有state getters actions mutations

3、切记在导出模块的时候 加一个 namespaced:true 主要的作用是将每个模块都有独立命名空间

4、namespace：true

在多人协作开发的时候，由于个人单词量有限，可能子模块和主模块中的函数名字会相同，这样调用函数的时候，相同名字的函数都会被调用，就会发生问题。

为了解决这个问题，上面提到了导出模块的时候要加namespace：true.





例如 moduleA.js

```javascript
const state = {
    userName:'啦啦啦'
}
const mutations = {
    changeName(state){
        state.userName = 'hahahahahahahhaahha'
    }
}
const actions = {
    change_name({commit}){
        commit('changeName')
    }
}
const getters = {

}

export default {
    state,
    mutations,
    actions,
    getters
}
```

然后在store总的index.js文件中 

```javascript
import moduleA from './moduleA'

....
modules:{
  moduleA:moduleA
}
```

然后在vue页面中

```javascript
//html
 {{name}}

//JS
 name(){
     return  this.$store.state.moduleA.userName
 }
```





**暂时整理不完，待定，Loading.......**