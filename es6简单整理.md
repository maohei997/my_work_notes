## ES6

es6新特性很多在这只解释说明比较常用的，不常用的只提一笔

#### 1. 类（class）

现在框架常用的就是构造类，在此不详细说明。

#### 2. 模块化 (Module)

导入导出  

````javascript
a.js 中

export var name = '张三';
========================================= ①
test.js 中
var name = '张三'
var age = '24'
export {name,age}
========================================= ②
testDefault.js 中
var name = '李四'

export default name;
========================================= ③

b.js 中
import {name} from 'a'
import {name,age} from 'test'
import hahaha from 'testDefault'
````

**汇总 : 在 a.js 、 test.js中  不加default导出的 在引入的时候要使用 import {  }   对应键名进行导入,在testDefault.js中  使用的export default xxx 这种导出方式，在其他文件引入的时候 可以使用 import  任意名   from  '文件' ; 进行 导入**

#### 3.箭头函数

一直在用不做过多说明

#### 4.函数默认值

````javascript
function a (x=1,y=2) {
	//其他要干的操作
    .....
}

如果不传值 默认关于x , y 的逻辑操作就是 按照 1 ， 2 来进行，如果传入新值那么就是用新值来进行逻辑处理
````

#### 5.模板字符串

````javascript

不使用模板字符串

var desc = '张三的名字是' + name + '年龄是' + age;

使用模板字符串

var desc = `张三的名字是${name}年龄是${age}`

可以在${} 中进行逻辑运算
````

#### 6.解构赋值  （重要）

**数组中**

````javascript
var arr = ['one','two','three','four','five']

var [one,two,three] = foo

console.log(one);//'one'
console.log(two);//'two'
console.log(three);//'three'

//如果要忽略某些值
var [first, , , ,last] = foo;
console.log(first) = foo //one
console.log(last) = foo //last

//也可以
var a , b;
[a,b] = [1,2]
console.log(a) //1
console.log(b) //2


//没有对应值可以设置默认值
var a, b;

[a=5, b=7] = [1];
console.log(a); // 1
console.log(b); // 7



//交换两个变量的值
//以往 
var a = 1;
var b = 3;
var temp;
temp = a;
a = b
b = temp;
console.log(a,b,temp) // 3 1 1

//现在
var a = 1;
var b = 3;

[a, b] = [b, a];
console.log(a); // 3
console.log(b); // 1
````

**对象中**

````javascript
const obj = {
    a:1,
    b:2,
    c:3
}
let {a,b,c} = obj
console.log(a,b,c) // 1 2 3
````

#### 7 展开运算符（重要）

**用法很广**

````javascript
函数中
function sum(x,y,z){
    return x+y+z;
}
const numArr = [1,2,3]
//不用...
console.log(sum.apply(null,numArr))//6
//使用
console.log(sum(...numArr)) //6


````

````javascript
数组中
const arr = [1,2]

const arr1 = [...arr,3,4,5]

console.log(arr1) //[1,2,3,4,5]



//可用于数组浅拷贝  类似于Object.assign() 只遍历一层
var arr = [1,2,3]

var arr1 = [...arr]
arr2.push(4)

console.log(arr2,arr)//[1,2,3,4]   [1,2,3]


//多个数组合并
var arr = [1,2,3]

var arr1 = [4,5,6]

var arr3 = [...arr,...arr1]

console.log(arr3) //[1,2,3,4,5,6]

//相当于
var arr4 = arr.concat(arr1) // [1,2,3,4,5,6]

````

````javascript
对象中
var obj1 = {a:1,b:2}

var obj2 = {c:3,d:4}

var cloneObj = {...obj1,...obj2}

console.log(cloneObj) //{ a: 1, b: 2, c: 3, d: 4 }

在React中修改state的值  只想改某个值b  假设obj:{a:1,b:2}
this.setState({
    obj:{...this.state.obj,b:3}
})
````

#### 8. 对象属性简写

````javascript
const name = '李四',age = 18
const obj = {
    name,
    age
}

没啥可说的，简单一笔带过
````

#### 9. Promise   (重要)

promise 翻译就是 承诺  就是我承诺你的异步代码是同步执行的 **避免回调地狱**

````javascript
简单封装一个promise

function getData(){
    return new Promise((resolve,reject) => {
        //进行了网络请求
        axios.post('xxxx',xxx,xxx).then(res => {
            if(res){
                resolve(res)
            }else{
                reject('没有数据')
            }
        })
    })
}


//调用这个方法
getData().then(res =>{
    console.log(res)  ///这里的res就是resolve出来的值  
}).catch(err => {
    console.log(err) //这里的err 就是reject出来的值 '没有数据'
})
````

#### 10. let const

涉及到作用域 声明提升  常量问题 不做过多解释

````javascript
{
    var a = 10
}
console.log(a)  //10

{
    let a = 10
}
console.log(a) //a is not defined

a = 10;
var a;
console.log(a) //10

a = 10;
let a;
console.log(a) // 报错Cannot access 'a' before initialization     let const没有声明提升

const a = 'lll'
a = 'bbb'  //报错  Assignment to constant variable. 定义常量不允许修改 ,但是定义对象可以修改
````





